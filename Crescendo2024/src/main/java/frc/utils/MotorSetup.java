package frc.utils;

import com.revrobotics.CANSparkBase.IdleMode;

import edu.wpi.first.wpilibj.CAN;

import com.revrobotics.CANSparkMax;

/**
 * A class for the purpose of setting up a motor.
 */
public class MotorSetup {

    /**
     * Sets all the settings a motor should have set when instantiated.
     * It is reccomended all the non-motor paramaters are constants.
     * Run finalizeMotor on the motor after calling this to confirm the settings.
     * 
     * @param motor the motor to set up.
     * @param idleMode what the motor does when idle. IdleMode.kCoast or IdleMode.kBrake.
     * @param currentLimit the maximum amps to give the motor. 40 is reccomended.
     * @param rampRate the minimum number of seconds ramp up to maximum amps.
     * @param inverted whether or not the motor should be inverted.
     */
    public static void setupMotor(CANSparkMax motor, IdleMode idleMode, int currentLimit, double rampRate, boolean inverted) {
        motor.restoreFactoryDefaults();
        motor.setIdleMode(idleMode);
        motor.setSmartCurrentLimit(currentLimit);
        motor.setOpenLoopRampRate(rampRate);
        motor.setInverted(inverted);
    }

    /**
     * Sets all the settings a motor should have set when instantiated.
     * It is reccomended all the non-motor paramaters are constants.
     * Run finalizeMotor on the motor after calling this to confirm the settings.
     * 
     * @param motor the motor to set up.
     * @param idleMode what the motor does when idle. IdleMode.kCoast or IdleMode.kBrake.
     * @param currentLimit the maximum amps to give the motor. 40 is reccomended.
     * @param rampRate the minimum number of seconds ramp up to maximum amps.
     * The motor will not be inverted.
     */
    public static void setupMotor(CANSparkMax motor, IdleMode idleMode, int currentLimit, double rampRate) {
        setupMotor(motor, idleMode, currentLimit, rampRate, false);
    }

    /**
     * Sets all the settings a motor should have set when instantiated, and sets it to follow another motor.
     * It is reccomended all the paramaters are constants.
     * Run finalizeMotor on the motor after calling this to confirm the settings.
     * 
     * @param motor the motor to set up.
     * @param currentLimit the maximum amps to give the motor. 40 is reccomended.
     * @param rampRate the minimum number of seconds ramp up to maximum amps.
     * @param leader the motor to follow.
     * @param inverted whether or not the follower should be inverted.
     */
    public static void setupFollowerMotor(CANSparkMax follower, int currentLimit, double rampRate, CANSparkMax leader, boolean inverted) {
        follower.restoreFactoryDefaults();
        follower.setSmartCurrentLimit(currentLimit);
        follower.setOpenLoopRampRate(rampRate);
        follower.follow(leader, inverted);
    }

    /**
     * Sets all the settings a motor should have set when instantiated, and sets it to follow another motor.
     * It is reccomended all the non-motor paramaters are constants.
     * Run finalizeMotor on the motor after calling this to confirm the settings.
     * 
     * @param motor the motor to set up.
     * @param currentLimit the maximum amps to give the motor. 40 is reccomended.
     * @param rampRate the minimum number of seconds ramp up to maximum amps.
     * @param leader the motor to follow.
     * The follower will not be inverted.
     */
    public static void setupFollowerMotor(CANSparkMax follower, int currentLimit, double rampRate, CANSparkMax leader) {
        follower.restoreFactoryDefaults();
        follower.setSmartCurrentLimit(currentLimit);
        follower.setOpenLoopRampRate(rampRate);
        follower.follow(leader, false);
    }
    
    /**
     * Sets up both a leader and a follower motor.
     * It is reccomended all the non-motor paramaters are constants.
     * Make sure to run finilizeMotors on the motors after calling this to confirm the settings.
     * 
     * @param leader the leader motor.
     * @param leaderInverted whether or not the leader should be inverted.
     * @param follower the follower motor.
     * @param followerInverted whether or not the follower should be inverted.
     * @param idleMode the idle mode of both motors.
     * @param currentLimit the current limit of both motors.
     * @param rampRate the ramp rate of both motors.
     */
    public static void setupLeaderAndFollowerMotors(CANSparkMax leader, boolean leaderInverted, CANSparkMax follower, boolean followerInverted, IdleMode idleMode, int currentLimit, double rampRate) {
        setupMotor(leader, idleMode, currentLimit, rampRate, leaderInverted);
        setupFollowerMotor(follower, currentLimit, rampRate, leader, followerInverted);
    }

    /**
     * Sets up a PID controller for a motor
     * 
     * @param pidController the PID Controller
     * @param pidSlot the controller slot on the SparkMAX
     * @param P the proportional value
     * @param I the integral value
     * @param D the derivative value
     * @param FF the feed-forward value
     * @param IZone the range of error for the integral value to take
	 * @param outputMin the minimum value the PID controller can output
	 * @param outputMax the maximum value the PID controller can output
     */
    public static void setupPIDController(SparkPIDController pidController, int pidSlot, double P, double I, double D, double FF, double IZone, double outputMin, double outputMax) {
        pidController.setP(P,pidSlot);
        pidController.setI(I,pidSlot);
        pidController.setD(D,pidSlot);
        pidController.setFF(FF,pidSlot);
        pidController.setIZone(IZone,pidSlot);
        pidController.setOutputRange(outputMin, outputMax, pidSlot);

    }
	
	/**
     * Sets up a PID controller for a motor
     * 
     * @param pidController the PID Controller
     * @param pidSlot the controller slot on the SparkMAX
     * @param P the proportional value
     * @param I the integral value
     * @param D the derivative value
     * @param FF the feed-forward value
     * @param IZone the range of error for the integral value to take
     */
    public static void setupPIDController(SparkPIDController pidController, int pidSlot, double P, double I, double D, double FF, double IZone) {
        setupPIDController(pidController, pidSlot, P, I, D, FF, IZone, -1.0, 1.0);
    }
	
	/**
     * Sets up a PID controller for a motor
     * 
     * @param pidController the PID Controller
     * @param pidSlot the controller slot on the SparkMAX
     * @param P the proportional value
     * @param I the integral value
     * @param D the derivative value
     * @param FF the feed-forward value
     */
    public static void setupPIDController(SparkPIDController pidController, int pidSlot, double P, double I, double D, double FF) {
        setupPIDController(pidController, pidSlot, P, I, D, FF, 0, -1.0, 1.0);
    }
	
	/**
     * Sets up a PID controller for a motor
     * 
     * @param pidController the PID Controller
     * @param pidSlot the controller slot on the SparkMAX
     * @param P the proportional value
     * @param I the integral value
     * @param D the derivative value
     */
    public static void setupPIDController(SparkPIDController pidController, int pidSlot, double P, double I, double D) {
        setupPIDController(pidController, pidSlot, P, I, D, 0, 0, -1.0, 1.0);
    }

    /**
     * Finalizes your motor settings by running burnFlash() on the motor you pass in.
     * This effectively burns the current settings of the motor into it's mechanical memory.
     * It's good practice to do this so the settings aren't lost when the robot turns off.
     * 
     * @param motor the motor to finalize.
     */
    public static void finalizeMotor(CANSparkMax motor) {
        motor.burnFlash();
    }

    /**
     * Finalizes your motor settings by running burnFlash() on the motors you pass in.
     * This effectively burns the current settings of the motor into it's mechanical memory.
     * It's good practice to do this so the settings aren't lost when the robot turns off.
     * 
     * @param motors the motors to finalize.
     */
    public static void finalizeMotors(CANSparkMax... motors) {
        for(CANSparkMax current:motors) current.burnFlash();
    }
}
