package frc.utils;

import com.ctre.phoenix6.configs.Pigeon2Configuration;
import com.ctre.phoenix6.hardware.Pigeon2;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import java.util.Optional;

/**
 * SwerveIMU interface for the Pigeon2
 */
public class Pigeon2Swerve
{

  /**
   * Pigeon2 IMU device.
   */
  Pigeon2 imu;
  /**
   * Offset for the Pigeon 2.
   */
  private Rotation3d offset = new Rotation3d();

  /**
   * Generate the SwerveIMU for pigeon.
   *
   * @param canid  CAN ID for the pigeon
   * @param canbus CAN Bus name the pigeon resides on.
   */
  public Pigeon2Swerve(int canid, String canbus)
  {
    imu = new Pigeon2(canid, canbus);
    Pigeon2Configuration config = new Pigeon2Configuration();
    imu.getConfigurator().apply(config);
    SmartDashboard.putData(imu);
  }

  /**
   * Generate the SwerveIMU for pigeon.
   *
   * @param canid CAN ID for the pigeon
   */
  public Pigeon2Swerve(int canid)
  {
    this(canid, "");
  }

  /**
   * Reset IMU to factory default.
   */
  public void factoryDefault()
  {
    imu.getConfigurator().apply(new Pigeon2Configuration());
  }

  /**
   * Clear sticky faults on IMU.
   */
  public void clearStickyFaults()
  {
    imu.clearStickyFaults();
  }

  /**
   * Set the gyro offset.
   *
   * @param offset gyro offset as a {@link Rotation3d}.
   */
  public void setOffset(Rotation3d offset)
  {
    this.offset = offset;
  }

  /**
   * Fetch the {@link Rotation3d} from the IMU without any zeroing. Robot relative.
   *
   * @return {@link Rotation3d} from the IMU.
   */
  public Rotation3d getRawRotation3d()
  {
    return imu.getRotation3d();
  }

  /**
   * Fetch the {@link Rotation3d} from the IMU. Robot relative.
   *
   * @return {@link Rotation3d} from the IMU.
   */
  public Rotation3d getRotation3d()
  {
    return getRawRotation3d().minus(offset);
  }

  /**
   * Fetch the acceleration [x, y, z] from the IMU in meters per second squared. If acceleration isn't supported returns
   * empty.
   *
   * @return {@link Translation3d} of the acceleration as an {@link Optional}.
   */
  public Optional<Translation3d> getAccel()
  {
    return Optional.of(new Translation3d(
      imu.getAccelerationX().getValue(), 
      imu.getAccelerationY().getValue(), 
      imu.getAccelerationZ().getValue()).times(9.81 / 16384.0));
  }

  public Optional<Rotation3d> getAngularVel()
  {
    return Optional.of(new Rotation3d(
      Math.toRadians(imu.getAngularVelocityXDevice().getValue()),
      Math.toRadians(imu.getAngularVelocityYDevice().getValue()),
      Math.toRadians(imu.getAngularVelocityZDevice().getValue())));
  }

  /**
   * Get the instantiated IMU object.
   *
   * @return IMU object.
   */
  public Object getIMU()
  {
    return imu;
  }
}
