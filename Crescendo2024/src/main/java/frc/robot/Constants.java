// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.pathplanner.lib.util.HolonomicPathFollowerConfig;
import com.pathplanner.lib.util.PIDConstants;
import com.pathplanner.lib.util.ReplanningConfig;
import com.revrobotics.CANSparkBase.IdleMode;

import edu.wpi.first.math.Matrix;
import edu.wpi.first.math.VecBuilder;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.interpolation.InterpolatingDoubleTreeMap;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.numbers.N1;
import edu.wpi.first.math.numbers.N3;
import edu.wpi.first.math.util.Units;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean
 * constants. This class should not be used for any other purpose. All constants
 * should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
  public static final class FieldConstants {
    public static final Translation2d FIELD_SIZE = new Translation2d(16.54, 8.21);
    public static final Translation2d RED_SPEAKER = new Translation2d(16.3,5.549);
    public static final Translation2d BLUE_SPEAKER = new Translation2d(-0.038099999999999995,4.982717999999999); // FIXME
  }

  public static final class DriveConstants {
    // Driving Parameters - Note that these are not the maximum capable speeds of
    // the robot, rather the allowed maximum speeds
    public static final double kMaxSpeedMetersPerSecond = 6.36;
    public static final double kMaxAngularSpeed = 2 * Math.PI; // radians per second

    public static final double kDirectionSlewRate = 5.4; // radians per second
    public static final double kMagnitudeSlewRate = 5.4; // percent per second (1 = 100%)
    public static final double kRotationalSlewRate = 4.0; // percent per second (1 = 100%)
    
    public static final double kSpeakerTXTolerance = 1.0;

    // Chassis configuration
    public static final double kTrackWidth = Units.inchesToMeters(23.5);
    // Distance between centers of right and left wheels on robot
    public static final double kWheelBase = Units.inchesToMeters(23.5);
    // Distance between front and back wheels on robot
    public static final SwerveDriveKinematics kDriveKinematics = new SwerveDriveKinematics(
        new Translation2d(kWheelBase / 2, kTrackWidth / 2),
        new Translation2d(kWheelBase / 2, -kTrackWidth / 2),
        new Translation2d(-kWheelBase / 2, kTrackWidth / 2),
        new Translation2d(-kWheelBase / 2, -kTrackWidth / 2));

    // Angular offsets of the modules relative to the chassis in radians
    public static final double kFrontLeftChassisAngularOffset = -Math.PI / 2;
    public static final double kFrontRightChassisAngularOffset = 0;
    public static final double kBackLeftChassisAngularOffset = Math.PI;
    public static final double kBackRightChassisAngularOffset = Math.PI / 2;

    // SPARK MAX CAN IDs
    public static final int kFrontLeftDrivingCanId = 6;
    public static final int kRearLeftDrivingCanId = 8;
    public static final int kFrontRightDrivingCanId = 5;
    public static final int kRearRightDrivingCanId = 7;

    public static final int kFrontLeftTurningCanId = 2;
    public static final int kRearLeftTurningCanId = 4;
    public static final int kFrontRightTurningCanId = 1;
    public static final int kRearRightTurningCanId = 3;

    // Gyro constants
    public static  final int kPigeonCanId = 16;

    public static final boolean kGyroReversed = true;

    public static final boolean kChassisVelocityCorrection = false;
    public static final double kChassisVelocityDT = 0.2;

    public static final boolean kHeadingCorrection = false;
    public static final double kHeadingCorrectionP = 3;
    public static final double kHeadingCorrectionI = 0;
    public static final double kHeadingCorrectionD = 0;
    public static final double kHeadingCorrectionTolerance = Math.toRadians(0.1);

    public static final boolean kNoteTracking = true;
    public static final double kNoteTrackingP = 10;
    public static final double kNoteTrackingI = 0;
    public static final double kNoteTrackingD = 0;
    public static final double kNoteTrackingTolerance = Math.toRadians(0.0);

    public static final HolonomicPathFollowerConfig pathFollowerConfig = new HolonomicPathFollowerConfig(
            new PIDConstants(3.5, 0.0, 0), // Translation constants
            new PIDConstants(5.0, 0, 0), // Rotation constants
            kMaxSpeedMetersPerSecond,
            new Translation2d(kWheelBase / 2, kTrackWidth / 2).getNorm(), // Drive base radius (distance from center to furthest module)
            new ReplanningConfig()
    );
    public static final Matrix<N3, N1> stateStdDevs = VecBuilder.fill(0.1, 0.1, 0.05);
    public static final Matrix<N3, N1> visionStdDevs = VecBuilder.fill(0.9, 0.9, 0.9);
    public static final Matrix<N3, N1> visionStdDevsTrust = VecBuilder.fill(0.2, 0.2, 0.2);
  }

  public static final class ModuleConstants {
    // The MAXSwerve module can be configured with one of three pinion gears: 12T, 13T, or 14T.
    // This changes the drive speed of the module (a pinion gear with more teeth will result in a
    // robot that drives faster).
    public static final int kDrivingMotorPinionTeeth = 16;

    // Invert the turning encoder, since the output shaft rotates in the opposite direction of
    // the steering motor in the MAXSwerve Module.
    public static final boolean kTurningEncoderInverted = true;

    // Calculations required for driving motor conversion factors and feed forward
    public static final double kDrivingMotorFreeSpeedRps = NeoMotorConstants.kFreeSpeedRpm / 60;
    public static final double kWheelDiameterMeters = 0.0762;
    public static final double kWheelCircumferenceMeters = kWheelDiameterMeters * Math.PI;
    // 45 teeth on the wheel's bevel gear, 22 teeth on the first-stage spur gear, 15 teeth on the bevel pinion
    public static final double kDrivingMotorReduction = (45.0 * 19) / (kDrivingMotorPinionTeeth * 15);
    public static final double kDriveWheelFreeSpeedRps = (kDrivingMotorFreeSpeedRps * kWheelCircumferenceMeters)
        / kDrivingMotorReduction;

    public static final double kDrivingEncoderPositionFactor = (kWheelDiameterMeters * Math.PI)
        / kDrivingMotorReduction; // meters
    public static final double kDrivingEncoderVelocityFactor = ((kWheelDiameterMeters * Math.PI)
        / kDrivingMotorReduction) / 60.0; // meters per second

    public static final double kTurningEncoderPositionFactor = (2 * Math.PI); // radians
    public static final double kTurningEncoderVelocityFactor = (2 * Math.PI) / 60.0; // radians per second

    public static final double kTurningEncoderPositionPIDMinInput = 0; // radians
    public static final double kTurningEncoderPositionPIDMaxInput = kTurningEncoderPositionFactor; // radians

    public static final double kDrivingP = 0.04;
    public static final double kDrivingI = 0;
    public static final double kDrivingD = 0;
    public static final double kDrivingFF = 1 / kDriveWheelFreeSpeedRps;
    public static final double kDrivingMinOutput = -1;
    public static final double kDrivingMaxOutput = 1;

    public static final double kTurningP = 1;
    public static final double kTurningI = 0;
    public static final double kTurningD = 0;
    public static final double kTurningFF = 0;
    public static final double kTurningMinOutput = -1;
    public static final double kTurningMaxOutput = 1;

    public static final IdleMode kDrivingMotorIdleMode = IdleMode.kBrake;
    public static final IdleMode kTurningMotorIdleMode = IdleMode.kBrake;

    public static final int kDrivingMotorCurrentLimit = 50; // amps
    public static final int kTurningMotorCurrentLimit = 20; // amps
  }

  public static final class IntakeConstants {
    public static final int LEADER_MOTOR_ID = 9;
    public static final int FOLLOWER_MOTOR_ID = 10;
    public static final int SENSOR_ID = 0;

    public static final int CURRENT_LIMIT = 80; // amps
    public static final double RAMP_RATE = 0.25;
    public static final boolean LEADER_INVERTED = false;

    public static final double INTAKE_SPEED = 1; // TODO: find proper intake speed
  }
  
  public static final class AngleConstants {
    public static final int MOTOR_ID = 16;
    public static final int CURRENT_LIMIT = 40;
    public static final double RAMP_RATE = 0.05;
    public static final boolean INVERTED = true;

    public static final boolean ENCODER_INVERTED = false;
    public static final double PROTOTYPE_ZERO_OFFSET = 232;
    public static final double COMPETITION_ZERO_OFFSET = 359;
    public static final double ERROR_TOLERANCE = 0.5;
    
    public static final double P = 0.03;
    public static final double I = 0;
    public static final double D = 0.002;
    public static final double FF = 0;
    public static final double OUTPUT_RANGE_MIN = -1.0;
    public static final double OUTPUT_RANGE_MAX = 1.0;

    public static final double SUBWOOFER_ANGLE = 57;
    public static final double PODIUM_ANGLE = 30;
    public static final double WING_ANGLE = 20.5; //Hardstop is 20 degrees
    public static final double AMP_ANGLE = 40;

    public static final double MINIMUM_ANGLE = 20;
    public static final double MAXIMUM_ANGLE = 60; // FIXME

    public static final InterpolatingDoubleTreeMap ANGLE_INTERPOLATION = new InterpolatingDoubleTreeMap();
    static {
      ANGLE_INTERPOLATION.put(1.4,SUBWOOFER_ANGLE);
      ANGLE_INTERPOLATION.put(1.7, 47.0);
      ANGLE_INTERPOLATION.put(2.0, 39.0);
      ANGLE_INTERPOLATION.put(2.25, 35.0);
      ANGLE_INTERPOLATION.put(2.5, 34.0);
      ANGLE_INTERPOLATION.put(2.75, 32.6);
      ANGLE_INTERPOLATION.put(3.0, PODIUM_ANGLE);
      ANGLE_INTERPOLATION.put(3.5, 26.25);
      ANGLE_INTERPOLATION.put(4.0, 23.5);
      ANGLE_INTERPOLATION.put(4.5, 21.7);
      ANGLE_INTERPOLATION.put(7.3, WING_ANGLE);
    }
  }

  public static final class ShooterConstants {
    public static final int LEADER_MOTOR_ID = 14;
    public static final int FOLLOWER_MOTOR_ID = 15;
    public static final int SENSOR_ID = 2;

    public static final double SPEED = 5000.0; //FIXME and the two below
    public static final double POWER = 1.0;
    public static final double TARGET_RPM = 5000;

    public static final int CURRENT_LIMIT = 80; // amps
    public static final double RAMP_RATE = 0.25;
    public static final double P = 0.001; // TODO: turn PID controller's P, F and output range
    public static final double I = 0.0;
    public static final double D = 0.0;
    public static final double FF = 0.0001;
    public static final double OUTPUT_RANGE_MIN = -1.0;
    public static final double OUTPUT_RANGE_MAX = 1.0;
    public static final boolean LEADER_INVERTED = true;
    public static final boolean FOLLOWER_INVERTED = false;

    public static final double NOTE_VELOCITY = 8.0; //FIXME
  }

  public static final class KickerConstants {
    public static final int MOTOR_ID = 13;

    public static final boolean INVERTED = true;
    public static final int CURRENT_LIMIT = 40; // amps
    public static final double RAMP_RATE = 0.25; //TODO: test this

    public static final double KICKER_SPEED = 0.2; //TODO: find proper kicker speed
  }

  public static final class OIConstants {
    public static final int kDriverControllerPort = 0;
    public static final int kOperatorControllerPort = 1;
    public static final double kDriveDeadband = 0.05;
  }

  public static final class AutoConstants {
    public static final double kMaxSpeedMetersPerSecond = 4;
    public static final double kMaxAccelerationMetersPerSecondSquared = 3;
    public static final double kMaxAngularSpeedRadiansPerSecond = Math.PI;
    public static final double kMaxAngularSpeedRadiansPerSecondSquared = Math.PI;

    public static final double kPXYController = 1;
    public static final double kPThetaController = 1;

  }

  public static final class NeoMotorConstants {
    public static final double kFreeSpeedRpm = 5676;

  }

  public static final class ConveyorConstants {
    public static final double DEFAULT_CONVEYOR_SPEED = 1.00;
    public static final int LEADER_MOTOR_ID = 11;
    public static final int FOLLOWER_MOTOR_ID = 12;
    public static final int BEAM_BREAK_ID = 1;
    public static final int CURRENT_LIMIT = 40;
    public static final double RAMP_RATE = .25;
  }

  public static final class ClimberMotorConstants {
    public static final int LEFT_CLIMBER_MOTOR_CAN_ID = 18;
    public static final int RIGHT_CLIMBER_MOTOR_CAN_ID = 19;

    public static final double CLIMBER_MOTOR_UP_SPEED = .5;
    public static final double CLIMBER_MOTOR_DOWN_SPEED = -.5;
    public static final int CURRENT_LIMIT = 40;
    public static final double RAMP_RATE = 0.25;
  }

  public static final class VisionConstants {
    public static final int POSE_ESTIMATION = 0;
    public static final int SPEAKER_TRACKING = 1;
    public static final int NOTE_TRACKING = 2;
  }

  public static class LEDConstants {
    public static final int LED_PORT = 0;
    public static final int FULL_LENGTH = 36;
    public static final int SHOOTER_STRIP_LENGTH = 12;
    public static final int SHOOTER_LENGTH = 36;
    public static final double STROBE_FAST_DURATION = 0.5;
    public static final double STROBE_SLOW_DURATION = 1;
    public static final double BREATH_DURATION = 1.0;
    public static final double RAINBOW_CYCLE_LENGTH = 36.0;
    public static final double RAINBOW_DURATION = 3.0;
    public static final double WAVE_EXPONENT = 0.4;
    public static final double WAVE_FAST_CYCLE_LENGTH = 25.0;
    public static final double WAVE_FAST_DURATION = 0.25;
    public static final double WAVE_SLOW_CYCLE_LENGTH = 25.0;
    public static final double WAVE_SLOW_DURATION = 3.0;
    public static final double WAVE_ALLIANCE_CYCLE_LENGTH = 15.0;
    public static final double WAVE_ALLIANCE_DURATION = 2.0;
    public static final double AUTO_FADE_TIME = 2.5; // 3s nominal
    public static final double AUTO_FADE_MAX_TIME = 5.0; // Return to normal
  }
  
}
