package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.NetworkTableValue;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.utils.MotorSetup;

public class ClimberSubsystem extends SubsystemBase {
    private CANSparkMax climberMotorLeader;
    private CANSparkMax climberMotorFollower;
    
    public ClimberSubsystem ()
    {
        climberMotorLeader = new CANSparkMax(Constants.ClimberMotorConstants.RIGHT_CLIMBER_MOTOR_CAN_ID, MotorType.kBrushless);
        climberMotorFollower = new CANSparkMax(Constants.ClimberMotorConstants.LEFT_CLIMBER_MOTOR_CAN_ID, MotorType.kBrushless);
        setupMotors();
    } 

    public void setClimberMotorSpeed (double climber_motor_speed)
    {
        climberMotorLeader.set(climber_motor_speed);
    }

    public double getClimberMotorSpeed ()
    {
        return climberMotorLeader.get();
    }

    //climber arms lift up
    public void climberUp ()
    {
       setClimberMotorSpeed(Constants.ClimberMotorConstants.CLIMBER_MOTOR_UP_SPEED);
    }

    //climber arms lower
    public void climberDown ()
    {
        setClimberMotorSpeed(Constants.ClimberMotorConstants.CLIMBER_MOTOR_DOWN_SPEED);
    }

    //climber arms turn off
    public void climberOff()
    {
        setClimberMotorSpeed(0);
    }

    @Override
    public void periodic ()
    {
        NetworkTableInstance.getDefault()
            .getTable("Climber")
            .putValue("climberSpeed", NetworkTableValue.makeDouble(getClimberMotorSpeed()));
    }
    
    public void setupMotors()
    {
        MotorSetup.setupLeaderAndFollowerMotors(
            climberMotorLeader, false,
            climberMotorFollower, false,
            IdleMode.kBrake,
            Constants.ClimberMotorConstants.CURRENT_LIMIT,
            Constants.ClimberMotorConstants.RAMP_RATE);
        MotorSetup.finalizeMotors(climberMotorLeader, climberMotorFollower);
    }
}