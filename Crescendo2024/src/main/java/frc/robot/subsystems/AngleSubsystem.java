package frc.robot.subsystems;

import java.util.function.DoubleSupplier;

import com.revrobotics.AbsoluteEncoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkBase.ControlType;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.SparkAbsoluteEncoder.Type;
import com.revrobotics.SparkPIDController;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.AngleConstants;
import frc.utils.MotorSetup;

/**
 * The subsystem that will control the robot's flywheels and shooter angle.
 */
public class AngleSubsystem extends SubsystemBase {
    private final CANSparkMax m_motor;
    private final SparkPIDController m_PID;
    private final AbsoluteEncoder m_encoder;
    private final NetworkTable m_table;
    private double m_holdAngle;

    /**
     * Creates a new ShooterSubsystem.
     */
    public AngleSubsystem() {
        m_motor = new CANSparkMax(AngleConstants.MOTOR_ID, MotorType.kBrushless);
        m_PID = m_motor.getPIDController();
        m_encoder = m_motor.getAbsoluteEncoder(Type.kDutyCycle);
        m_holdAngle = m_encoder.getPosition();
        setUpMotors();

        NetworkTableInstance inst = NetworkTableInstance.getDefault();
        m_table = inst.getTable("Angle");
    }

    /**
     * Sets many motor settings.
     */
    private void setUpMotors() {
        MotorSetup.setupMotor(m_motor,
            IdleMode.kBrake,
            AngleConstants.CURRENT_LIMIT,
            AngleConstants.RAMP_RATE,
            AngleConstants.INVERTED);
        
        m_encoder.setPositionConversionFactor(360);
        // m_encoder.setZeroOffset(AngleConstants.ZERO_OFFSET);

        m_PID.setP(AngleConstants.P);
        m_PID.setI(AngleConstants.I);
        m_PID.setD(AngleConstants.D);
        m_PID.setFF(AngleConstants.FF);
        m_PID.setIZone(0);
        m_PID.setOutputRange(AngleConstants.OUTPUT_RANGE_MIN, AngleConstants.OUTPUT_RANGE_MAX);
        m_PID.setFeedbackDevice(m_encoder);

        MotorSetup.finalizeMotor(m_motor);
    }

    /**
     * Sets the target angle of the PID controller, within bounds.
     * 
     * @param angle the angle, in degrees, to turn to.
     */
    public void setAngle(double angle) {
        m_holdAngle = angle > AngleConstants.MAXIMUM_ANGLE ? AngleConstants.MAXIMUM_ANGLE :
            (angle < AngleConstants.MINIMUM_ANGLE ? AngleConstants.MINIMUM_ANGLE : angle);
        m_PID.setReference(m_holdAngle, ControlType.kPosition);
    }

    /**
     * Returns whether or not the angle has reached its target.
     * 
     * @return whether the angle has reached its target.
     */
    public boolean atTarget() {
        return Math.abs(m_encoder.getPosition()-m_holdAngle)<AngleConstants.ERROR_TOLERANCE;
    }

    @Override
    public void periodic() {
        m_table.getEntry("angle").setDouble(m_encoder.getPosition());
        m_table.getEntry("hold angle").setDouble(m_holdAngle);
    }

    /**
     * Returns a command that sets the target angle in degrees for the angle.
     * 
     * @param angle the angle, in degrees, to turn to.
     * 
     * @return the command.
     */
    public Command setAngleCommand(double angle) {
        return this.run(() -> setAngle(angle));
    }

    /**
     * Sets the angle based on the distance to our speaker using the interpolation table in constants.
     * 
     * @param distance the distance, in meters, from the speaker.
     * 
     * @return the command.
     */
    public Command setSmartAngleCommand(DoubleSupplier distance) {
        return this.run(() -> setAngle(AngleConstants.ANGLE_INTERPOLATION.get(distance.getAsDouble())));
    }
}
