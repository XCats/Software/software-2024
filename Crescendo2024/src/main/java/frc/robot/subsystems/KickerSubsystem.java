package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.NetworkTableValue;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import frc.robot.Constants.KickerConstants;
import frc.utils.MotorSetup;

public class KickerSubsystem extends SubsystemBase {
    private CANSparkMax m_kickerMotor;

    public KickerSubsystem() {
        m_kickerMotor = new CANSparkMax(KickerConstants.MOTOR_ID, MotorType.kBrushless);
        setUpMotors();
    }

    private void setUpMotors() {
        MotorSetup.setupMotor(m_kickerMotor,
            IdleMode.kBrake,
            KickerConstants.CURRENT_LIMIT,
            KickerConstants.RAMP_RATE,
            KickerConstants.INVERTED);

        MotorSetup.finalizeMotor(m_kickerMotor);
    }

    public void kickerOn() {
        m_kickerMotor.set(KickerConstants.KICKER_SPEED);
    }

    public void kickerOff() {
        m_kickerMotor.set(0);
    }

    @Override
    public void periodic()
    {
        NetworkTableInstance.getDefault()
            .getTable("Kicker")
            .putValue("Kicker motor speed", NetworkTableValue.makeDouble( m_kickerMotor.get()));
    }
}
