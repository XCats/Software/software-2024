package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.NetworkTableValue;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.utils.MotorSetup;

public class ConveyorSubsytem extends SubsystemBase {
    private CANSparkMax lConveyorMotor;
    private CANSparkMax fConveyorMotor;
    private DigitalInput m_beamBreak;
    
    public ConveyorSubsytem()
    {
        lConveyorMotor = new CANSparkMax(Constants.ConveyorConstants.LEADER_MOTOR_ID, MotorType.kBrushless);
        fConveyorMotor = new CANSparkMax(Constants.ConveyorConstants.FOLLOWER_MOTOR_ID, MotorType.kBrushless);
        m_beamBreak = new DigitalInput(Constants.ConveyorConstants.BEAM_BREAK_ID);
        setUpMotors();
    } 

    public void setUpMotors() {
        MotorSetup.setupLeaderAndFollowerMotors(
            lConveyorMotor, false,
            fConveyorMotor, false,
            IdleMode.kBrake,
            Constants.ConveyorConstants.CURRENT_LIMIT,
            Constants.ConveyorConstants.RAMP_RATE);
        MotorSetup.finalizeMotors(lConveyorMotor, fConveyorMotor);
    }

    public void setConveyorMotorSpeed(double userConveyorMotorSpeed)
    {
        lConveyorMotor.set(userConveyorMotorSpeed);
    }

    public double getConveyorMotorSpeed()
    {
        return lConveyorMotor.get();
    }

    public void stop()
    {
        setConveyorMotorSpeed(0);
    }

    public void convey()
    {
        setConveyorMotorSpeed(Constants.ConveyorConstants.DEFAULT_CONVEYOR_SPEED);
    }

    public boolean noteInConveyor()
    {
        return !m_beamBreak.get();
    }

    public boolean noteNotInConveyor()
    {
        return m_beamBreak.get();
    }

    @Override
    public void periodic()
    {
        NetworkTableInstance
            .getDefault()
            .getTable("Conveyor")
            .putValue("ConveyorSpeed", NetworkTableValue.makeDouble(getConveyorMotorSpeed()));
    }
}
