package frc.robot.subsystems;

import com.revrobotics.CANSparkBase;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.SparkPIDController;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.ShooterConstants;
import frc.utils.MotorSetup;

/**
 * The subsystem that will control the robot's flywheels and shooter angle.
 */
public class ShooterSubsystem extends SubsystemBase {
    private final CANSparkMax m_leaderMotor;
    private final CANSparkMax m_followerMotor;
    private final RelativeEncoder m_leaderEncoder;
    private final RelativeEncoder m_followerEncoder;
    private final SparkPIDController m_PID;
    private final DigitalInput m_beamBreak;
    private final NetworkTable m_table;

    /**
     * Creates a new ShooterSubsystem.
     */
    public ShooterSubsystem() {
        m_leaderMotor = new CANSparkMax(ShooterConstants.LEADER_MOTOR_ID, MotorType.kBrushless);
        m_followerMotor = new CANSparkMax(ShooterConstants.FOLLOWER_MOTOR_ID, MotorType.kBrushless);
        m_PID = m_leaderMotor.getPIDController();
        setUpMotors();
        m_leaderEncoder = m_leaderMotor.getEncoder();
        m_followerEncoder = m_followerMotor.getEncoder();
        m_beamBreak = new DigitalInput(ShooterConstants.SENSOR_ID);

        NetworkTableInstance inst = NetworkTableInstance.getDefault();
        m_table = inst.getTable("Shooter");

    }

    /**
     * Sets many motor settings.
     */
    private void setUpMotors() {
        MotorSetup.setupLeaderAndFollowerMotors(
            m_leaderMotor, ShooterConstants.LEADER_INVERTED,
            m_followerMotor, ShooterConstants.FOLLOWER_INVERTED,
            IdleMode.kCoast,
            ShooterConstants.CURRENT_LIMIT,
            ShooterConstants.RAMP_RATE);

        m_PID.setP(ShooterConstants.P,0);
        m_PID.setI(ShooterConstants.I,0);
        m_PID.setD(ShooterConstants.D,0);
        m_PID.setFF(ShooterConstants.FF,0);
        m_PID.setIZone(0,0);
        m_PID.setOutputRange(ShooterConstants.OUTPUT_RANGE_MIN, ShooterConstants.OUTPUT_RANGE_MAX);

        MotorSetup.finalizeMotors(m_leaderMotor, m_followerMotor);
    }

    /**
     * Turns on the shooter flywheels to the specified speed.
     * 
     * @param speed the speed in revolutions per minute to set the motor to. TODO: check units
     */
    public void setSpeed(double speed) {
        m_PID.setReference(speed, CANSparkBase.ControlType.kVelocity);
    }

    /**
     * Turns on the shooter flywheels to the specified power.
     * 
     * @param power the percentage power
     */
    public void setPower(double power) {
        m_PID.setReference(power, CANSparkBase.ControlType.kDutyCycle, 0);
    }

    /**
     * Turns off the shooter flywheels.
     * 
     */
    public void shooterOff() {
        m_leaderMotor.stopMotor();
    }

    /**
     * Returns true if the shooter does not have a note.
     * If it started with a note, this being true means its been shot.
     * 
     * @return true if note detected.
     */
    public boolean shot() {
        return m_beamBreak.get();
    }

    /**
     * Returns true if there is a note ready to be shot.
     * 
     * @return true if note detected.
     */
    public boolean noteReadyToShoot() {
        return !m_beamBreak.get();
    }

    /**
     * Returns whether the shooter is up to speed.
     * 
     * @return whether the shooter is up to speed.
     */
    public boolean upToSpeed() {
        return (m_leaderEncoder.getVelocity() > ShooterConstants.TARGET_RPM) && (m_followerEncoder.getVelocity() > ShooterConstants.TARGET_RPM);
    }

    @Override
    public void periodic() {
        m_table.getEntry("Shooter leader speed").setDouble(m_leaderEncoder.getVelocity());
        m_table.getEntry("Shooter follower speed").setDouble(m_followerEncoder.getVelocity());
        m_table.getEntry("upToSpeed").setBoolean(upToSpeed());
    }

    /**
     * Returns a command that turns on the shooter.
     * 
     * @return the command.
     */
    public Command runShooterCommand() {
        return this.run(() -> setPower(ShooterConstants.POWER));
    }

    /**
     * Returns a command that turns off the shooter.
     * 
     * @return the command.
     */
    public Command stopShooterCommand() {
        return this.run(() -> shooterOff());
    }
}
