package frc.robot.subsystems;

import java.util.Optional;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.VisionConstants;
import frc.utils.LimelightHelpers;

public class VisionSubsystem extends SubsystemBase{
    /** 
    public static enum Limelight {
        LEFT,
        RIGHT,
        NOTE;
    
        public String toString() {
          switch (this) {
            case LEFT:
              return "limelight-left";
            case RIGHT:
              return "limelight-right";
            case NOTE:
              return "limelight-note";
            default:
              return "";
          }
        }
    
        public static enum Pipeline {
            SPEAKER_TRACKING,
            POSE_ESTIMATION,
            NOTE_TRACKING;
        
            private int getIndex() {
            switch (this) {
                case SPEAKER_TRACKING:
                return 1;
                case POSE_ESTIMATION:
                return 0;
                case NOTE_TRACKING:
                return 0;
                default:
                return -1;
                }
            }
        }
    }*/

    private class Limelight{
        private final String m_Name;
        private int m_Pipeline;
        private double m_tx;
        private double m_tID;
        private Pose2d m_Pose;
        private double m_Timestamp;
        private boolean m_newMessage;
        private boolean m_changedPipeline;

        public Limelight(String name, int pipeline)
        {
            m_Name = name;
            m_tx = 0;
            m_Pose = new Pose2d(0,0, new Rotation2d(0));
            m_Pipeline = pipeline;
            m_newMessage = true;
            m_changedPipeline = false;
            setPipeline(m_Pipeline);
        }

        private void setPipeline(int index)
        {
            m_Pipeline = index;
            m_changedPipeline = true;
            LimelightHelpers.setPipelineIndex(m_Name, index);
        }

        private int getPipeline()
        {
            return (int)(LimelightHelpers.getCurrentPipelineIndex(m_Name)+0.5);
        }

        private boolean isNewMessage()
        {
            return m_newMessage;
        }

        private boolean isPipelineReady()
        {
            return !m_changedPipeline;
        }

        private int getTagID()
        {
            return (int)(LimelightHelpers.getFiducialID(m_Name)+0.5);
        }

        private double getTX()
        {
            return LimelightHelpers.getTX(m_Name);
        }

        private Pose2d getBotPose2d(Optional<Alliance> alliance)
        {
            try 
            {
                switch (alliance.get())
                {
                    case Red:
                        return LimelightHelpers.getBotPose2d_wpiRed(m_Name);
                    case Blue:
                        return LimelightHelpers.getBotPose2d_wpiBlue(m_Name);
                    default:
                        return LimelightHelpers.getBotPose2d(m_Name);

                }
            } catch (Exception e) {
                return LimelightHelpers.getBotPose2d(m_Name);
            }
        }

        private Optional<Double> getFilterIDTX(int[] filters)
        {
            // int getTagID() = 0;
            for(int filterID : filters)
            {
                // if 
            }
            return Optional.empty();
        }

        public void updateTelemetry()
        {
            m_Timestamp = Timer.getFPGATimestamp();
            if(m_Pipeline == getPipeline())
            {
                switch(m_Pipeline)
                {
                    case VisionConstants.POSE_ESTIMATION:
                        //m_Pose = getBotPose2d(DriverStation.getAlliance())
                        Pose2d tempPose = getBotPose2d(Optional.empty());
                        if(Math.abs(tempPose.getX() - m_Pose.getX())>=0.01)
                        {
                            m_changedPipeline = false;
                            m_newMessage = true;
                            m_Pose = tempPose;
                            break;
                        }
                        m_newMessage = false;
                        break;
                    case VisionConstants.SPEAKER_TRACKING:
                        double tempSpeaker = getTX();
                        if(Math.abs(tempSpeaker - m_tx) >= 0.1)
                        {
                            m_changedPipeline = false;
                            m_newMessage = true;
                            m_tx = tempSpeaker;
                            break;
                        }
                        m_newMessage = false;
                        break;
                    case VisionConstants.NOTE_TRACKING:
                        double tempNote = getTX();
                        if(Math.abs(tempNote - m_tx) >= 0.1)
                        {
                            m_changedPipeline = false;
                            m_newMessage = true;
                            m_tx = tempNote;
                            break;
                        }
                        m_newMessage = false;
                        break;
                }
            }
        }
    }

    private static final int LEFT = 0;
    private static final int RIGHT = 1;
    private static final int NOTE = 2;
    public final Limelight[] cams;

    public VisionSubsystem()
    {
        cams = new Limelight[]
        {
          new Limelight("limelight-left", VisionConstants.POSE_ESTIMATION),
          new Limelight("limelight-right", VisionConstants.POSE_ESTIMATION),
          new Limelight("limelight-note", VisionConstants.NOTE_TRACKING)
        };
    }

    public double getNoteTX()
    {
        Limelight cam = cams[NOTE];
        return cam.m_tx;
    }
    
    public Optional<Double> getSpeakerTX()
    {
        Limelight left = cams[LEFT];
        Limelight right = cams[RIGHT];

        double average = 0;
        int count = 0;
        int leftID = left.getTagID();
        int rightID = right.getTagID();
        if(leftID == 4 || leftID == 8)
        {
            average += left.m_tx;
            ++count;
        }
        if(rightID == 4 || rightID == 8)
        {
            average += right.m_tx;
            ++count;
        }
        if(count > 0)
        {
            return Optional.of(Math.toRadians(average /= count));
        }
        return Optional.empty();
    }

    public Optional<Pose2d> getLeftPose2d()
    {
        Limelight left = cams[LEFT];
        if(left.isPipelineReady())
        {
            return Optional.of(left.m_Pose);
        }
        return Optional.empty();
    }

    public Optional<Pose2d> getRightPose2d()
    {
        Limelight right = cams[RIGHT];
        if(right.isPipelineReady())
        {
            return Optional.of(right.m_Pose);
        }
        return Optional.empty();
    }

    public void setSpeakerTracking()
    {
        if(!isSpeakerTracking())
        {
            cams[LEFT].setPipeline(VisionConstants.SPEAKER_TRACKING);
            cams[RIGHT].setPipeline(VisionConstants.SPEAKER_TRACKING);
        }
    }

    public void setPoseEstimation()
    {
        if(!isPoseEstimating())
        {
            cams[LEFT].setPipeline(VisionConstants.POSE_ESTIMATION);
            cams[RIGHT].setPipeline(VisionConstants.POSE_ESTIMATION);  
        }  
    }

    public boolean isSpeakerTracking()
    {
        return (cams[LEFT].getPipeline() == VisionConstants.SPEAKER_TRACKING)
            && (cams[RIGHT].getPipeline() == VisionConstants.SPEAKER_TRACKING)
            && (cams[LEFT].isPipelineReady())
            && (cams[RIGHT].isPipelineReady());
    }

    public boolean isPoseEstimating()
    {
        return (cams[LEFT].getPipeline() == VisionConstants.POSE_ESTIMATION)
            && (cams[RIGHT].getPipeline() == VisionConstants.POSE_ESTIMATION)
            && (cams[LEFT].isPipelineReady())
            && (cams[RIGHT].isPipelineReady());
    }

    public void periodic(){
        for(Limelight cam : cams)
        {
            cam.updateTelemetry();
        }
        SmartDashboard.putNumber("left pipe", cams[LEFT].m_Pipeline);
        try
        {
            SmartDashboard.putNumber("Speaker TX", Math.toDegrees(getSpeakerTX().get()));
        }
        catch (Exception e)
        {
            //catch
        }
    }
}

