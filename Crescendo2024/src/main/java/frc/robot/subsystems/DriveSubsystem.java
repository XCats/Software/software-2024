// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.pathplanner.lib.auto.AutoBuilder;

import edu.wpi.first.math.Matrix;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.estimator.SwerveDrivePoseEstimator;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.geometry.*;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveDriveOdometry;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.numbers.N1;
import edu.wpi.first.math.numbers.N3;
import edu.wpi.first.util.WPIUtilJNI;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import java.util.Optional;

import frc.robot.Constants;
import frc.robot.Constants.AngleConstants;
import frc.robot.Constants.DriveConstants;
import frc.robot.Constants.FieldConstants;
import frc.robot.Constants.ShooterConstants;
import frc.utils.Pigeon2Swerve;
import frc.utils.RobotPoseLookup;
import frc.utils.SwerveUtils;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.utils.LimelightHelpers;

public class DriveSubsystem extends SubsystemBase {
  
  // Create MAXSwerveModules
  private final MAXSwerveModule m_frontLeft = new MAXSwerveModule(
      DriveConstants.kFrontLeftDrivingCanId,
      DriveConstants.kFrontLeftTurningCanId,
      DriveConstants.kFrontLeftChassisAngularOffset);

  private final MAXSwerveModule m_frontRight = new MAXSwerveModule(
      DriveConstants.kFrontRightDrivingCanId,
      DriveConstants.kFrontRightTurningCanId,
      DriveConstants.kFrontRightChassisAngularOffset);

  private final MAXSwerveModule m_rearLeft = new MAXSwerveModule(
      DriveConstants.kRearLeftDrivingCanId,
      DriveConstants.kRearLeftTurningCanId,
      DriveConstants.kBackLeftChassisAngularOffset);

  private final MAXSwerveModule m_rearRight = new MAXSwerveModule(
      DriveConstants.kRearRightDrivingCanId,
      DriveConstants.kRearRightTurningCanId,
      DriveConstants.kBackRightChassisAngularOffset);

  // The gyro sensor
  private Pigeon2Swerve m_gyro = new Pigeon2Swerve(DriveConstants.kPigeonCanId);
  private final RobotPoseLookup poseLookup;

  //PID Controllers
  private final PIDController headingCorrectionController;
  private final PIDController noteTrackingController;

  private double lastHeadingRadians = 0;

  // Slew rate filter variables for controlling lateral acceleration
  private double m_currentRotation = 0.0;
  private double m_currentTranslationDir = 0.0;
  private double m_currentTranslationMag = 0.0;

  private SlewRateLimiter m_magLimiter = new SlewRateLimiter(DriveConstants.kMagnitudeSlewRate);
  private SlewRateLimiter m_rotLimiter = new SlewRateLimiter(DriveConstants.kRotationalSlewRate);
  private double m_prevTime = WPIUtilJNI.now() * 1e-6;

  // Odometry class for tracking robot pose
  SwerveDriveOdometry m_odometry = new SwerveDriveOdometry(
      DriveConstants.kDriveKinematics,
      getYaw(),
      getModulePositions()
  );

  SwerveDrivePoseEstimator m_poseEstimator = new SwerveDrivePoseEstimator(
          DriveConstants.kDriveKinematics,
          getYaw(),
          getModulePositions(),
          new Pose2d(new Translation2d(0, 0), Rotation2d.fromDegrees(0))
  );

  private final Field2d m_field = new Field2d();

  /** Creates a new DriveSubsystem. */
  public DriveSubsystem() {
    SmartDashboard.putData("Field", m_field);
    poseLookup = new RobotPoseLookup();
    m_gyro.factoryDefault();
    headingCorrectionController = new PIDController(DriveConstants.kHeadingCorrectionP, DriveConstants.kHeadingCorrectionI, DriveConstants.kHeadingCorrectionD);
    headingCorrectionController.enableContinuousInput(-Math.PI, Math.PI);
    headingCorrectionController.setTolerance(DriveConstants.kHeadingCorrectionTolerance);

    noteTrackingController = new PIDController(DriveConstants.kNoteTrackingP,DriveConstants.kNoteTrackingI, DriveConstants.kNoteTrackingD);
    noteTrackingController.enableContinuousInput(-Math.PI, Math.PI);
    noteTrackingController.setTolerance(DriveConstants.kNoteTrackingTolerance);

    zeroHeading();



    AutoBuilder.configureHolonomic(
            this::getAutoPose,
            this::resetAutoOdometry,
            this::getChassisSpeeds,
            this::setChassisSpeeds,
            DriveConstants.pathFollowerConfig,
            () -> {
              // Boolean supplier that controls when the path will be mirrored for the red alliance
              // This will flip the path being followed to the red side of the field.
              // THE ORIGIN WILL REMAIN ON THE BLUE SIDE

              var alliance = DriverStation.getAlliance();
              if (alliance.isPresent()) {
                return alliance.get() == DriverStation.Alliance.Red;
              }
              return false;
            },
            this
    );
  }

  @Override
  public void periodic() {
    // Update the odometry in the periodic block
    Pose2d currentPose = m_poseEstimator.update(getYaw(), getModulePositions());
    poseLookup.addPose(currentPose);
    SmartDashboard.putNumber("Pose X", currentPose.getX());
    SmartDashboard.putNumber("Pose Y", currentPose.getY());
    SmartDashboard.putNumber("Effective distance to Speaker", getEffectiveDistanceToSpeaker(ShooterConstants.NOTE_VELOCITY));
    correctOdom(false, false);
    m_field.setRobotPose(m_poseEstimator.getEstimatedPosition());
  }

  /**
   * Returns the currently-estimated pose of the robot.
   *
   * @return The pose.
   */
  public Pose2d getPose() {
    return m_poseEstimator.getEstimatedPosition();
  }

    /**
   * Returns the currently-estimated pose of the robot.
   *
   * @return The pose.
   */
  public Pose2d getAutoPose() {
    return m_odometry.getPoseMeters();
  }

  public ChassisSpeeds getChassisSpeeds() {
    return DriveConstants.kDriveKinematics.toChassisSpeeds(getModuleStates());
  }

  public SwerveModuleState[] getModuleStates() {
    SwerveModuleState[] states = new SwerveModuleState[4];
    states[0] = m_frontLeft.getState();
    states[1] = m_frontRight.getState();
    states[2] = m_rearLeft.getState();
    states[3] = m_rearRight.getState();
    return states;
  }

  /**
   * Resets the odometry to the specified pose.
   *
   * @param pose The pose to which to set the odometry.
   */
  public void resetOdometry(Pose2d pose) {
    m_poseEstimator.resetPosition(
        getYaw(),
        new SwerveModulePosition[] {
            m_frontLeft.getPosition(),
            m_frontRight.getPosition(),
            m_rearLeft.getPosition(),
            m_rearRight.getPosition()
        },
        pose);
  }

  /**
   * Resets the odometry to the specified pose.
   *
   * @param pose The pose to which to set the odometry.
   */
  public void resetAutoOdometry(Pose2d pose) {
    resetOdometry(pose);
    m_odometry.resetPosition(getYaw(), getModulePositions(), pose);
  }

  /**
   * Method to drive the robot using joystick info.
   *
   * @param xSpeed          Speed of the robot in the x direction (forward).
   * @param ySpeed          Speed of the robot in the y direction (sideways).
   * @param rot             Angular rate of the robot.
   * @param fieldRelative   Whether the provided x and y speeds are relative to the field.
   * @param rateLimit       Whether to enable rate limiting for smoother control.
   * @param trackingNote    Whether or not the robot should be turning to face a note it sees.
   * @param trackingSpeaker Whether or not the robot should be turning to face the speaker.
   */
  public void drive(double xSpeed, double ySpeed, double rot, boolean fieldRelative, boolean rateLimit, boolean trackingNote, boolean trackingSpeaker) {
    
    double xSpeedCommanded;
    double ySpeedCommanded;

    if (rateLimit) {
      // Convert XY to polar for rate limiting
      double inputTranslationDir = Math.atan2(ySpeed, xSpeed);
      double inputTranslationMag = Math.sqrt(Math.pow(xSpeed, 2) + Math.pow(ySpeed, 2));

      // Calculate the direction slew rate based on an estimate of the lateral acceleration
      double directionSlewRate;
      if (m_currentTranslationMag != 0.0) {
        directionSlewRate = Math.abs(DriveConstants.kDirectionSlewRate / m_currentTranslationMag);
      } else {
        directionSlewRate = 500.0; //some high number that means the slew rate is effectively instantaneous
      }
      

      double currentTime = WPIUtilJNI.now() * 1e-6;
      double elapsedTime = currentTime - m_prevTime;
      double angleDif = SwerveUtils.AngleDifference(inputTranslationDir, m_currentTranslationDir);
      if (angleDif < 0.45*Math.PI) {
        m_currentTranslationDir = SwerveUtils.StepTowardsCircular(m_currentTranslationDir, inputTranslationDir, directionSlewRate * elapsedTime);
        m_currentTranslationMag = m_magLimiter.calculate(inputTranslationMag);
      }
      else if (angleDif > 0.85*Math.PI) {
        if (m_currentTranslationMag > 1e-4) { //some small number to avoid floating-point errors with equality checking
          // keep currentTranslationDir unchanged
          m_currentTranslationMag = m_magLimiter.calculate(0.0);
        }
        else {
          m_currentTranslationDir = SwerveUtils.WrapAngle(m_currentTranslationDir + Math.PI);
          m_currentTranslationMag = m_magLimiter.calculate(inputTranslationMag);
        }
      }
      else {
        m_currentTranslationDir = SwerveUtils.StepTowardsCircular(m_currentTranslationDir, inputTranslationDir, directionSlewRate * elapsedTime);
        m_currentTranslationMag = m_magLimiter.calculate(0.0);
      }
      m_prevTime = currentTime;
      
      xSpeedCommanded = m_currentTranslationMag * Math.cos(m_currentTranslationDir);
      ySpeedCommanded = m_currentTranslationMag * Math.sin(m_currentTranslationDir);
      m_currentRotation = m_rotLimiter.calculate(rot);


    } else {
      xSpeedCommanded = xSpeed;
      ySpeedCommanded = ySpeed;
      m_currentRotation = rot;
    }

    // Convert the commanded speeds into the correct units for the drivetrain
    double xSpeedDelivered = xSpeedCommanded * DriveConstants.kMaxSpeedMetersPerSecond;
    double ySpeedDelivered = ySpeedCommanded * DriveConstants.kMaxSpeedMetersPerSecond;
    double rotDelivered = m_currentRotation * DriveConstants.kMaxAngularSpeed;

    Rotation2d heading = getYaw();
    if(DriveConstants.kChassisVelocityCorrection)
    {
      // heading.plus(Rotation2d.fromDegrees(getYawVel().getDegrees() * DriveConstants.kChassisVelocityDT));
      SmartDashboard.putNumber("Before Heading", heading.getDegrees());
      heading.plus(Rotation2d.fromDegrees(getYawVel().unaryMinus().getDegrees() * 0.1));
      SmartDashboard.putNumber("After Heading", heading.getDegrees());
    }
    ChassisSpeeds velocity = fieldRelative
    ? ChassisSpeeds.fromFieldRelativeSpeeds(xSpeedDelivered, ySpeedDelivered, rotDelivered, heading.unaryMinus())
    : new ChassisSpeeds(xSpeedDelivered, ySpeedDelivered, rotDelivered);
    if (trackingNote && Math.abs(rotDelivered) < 0.01)
    {
      velocity.omegaRadiansPerSecond = headingCorrectionController.calculate(Math.toRadians(LimelightHelpers.getTX("limelight-note"))+getYaw().getRadians(), getYaw().getRadians());
    }
    else if (trackingSpeaker && Math.abs(rotDelivered) < 0.01)
    {
      velocity.omegaRadiansPerSecond = headingCorrectionController.calculate(Math.toRadians(getAverageSpeakerTX())+getYaw().getRadians(), getYaw().getRadians());
    }
    else if (DriveConstants.kHeadingCorrection)
    {
      if (Math.abs(rotDelivered) < 0.01)
      {
        velocity.omegaRadiansPerSecond = headingCorrectionController.calculate(lastHeadingRadians, getYaw().getRadians());
      } 
      else
      {
        if(DriveConstants.kChassisVelocityCorrection)
        {
          // lastHeadingRadians = getYaw().getRadians() + getYawVel().getRadians() * DriveConstants.kChassisVelocityDT;
          lastHeadingRadians = getYaw().getRadians() + getYawVel().getRadians() * 0.05;
        }
        else
        {
          lastHeadingRadians = getYaw().getRadians();
        }
      }
    }
    var swerveModuleStates = DriveConstants.kDriveKinematics.toSwerveModuleStates(velocity);
    SwerveDriveKinematics.desaturateWheelSpeeds(
        swerveModuleStates, DriveConstants.kMaxSpeedMetersPerSecond);
    m_frontLeft.setDesiredState(swerveModuleStates[0], false);
    m_frontRight.setDesiredState(swerveModuleStates[1], false);
    m_rearLeft.setDesiredState(swerveModuleStates[2], false);
    m_rearRight.setDesiredState(swerveModuleStates[3], false);
  }

  /**
   * Sets the wheels into an X formation to prevent movement.
   */
  public void setX() {
    m_frontLeft.setDesiredState(new SwerveModuleState(0, Rotation2d.fromDegrees(45)), true);
    m_frontRight.setDesiredState(new SwerveModuleState(0, Rotation2d.fromDegrees(-45)), true);
    m_rearLeft.setDesiredState(new SwerveModuleState(0, Rotation2d.fromDegrees(-45)), true);
    m_rearRight.setDesiredState(new SwerveModuleState(0, Rotation2d.fromDegrees(45)), true);
  }

  /**
   * Set chassis speeds with closed-loop velocity control and second order kinematics.
   *
   * @param chassisSpeeds Chassis speeds to set.
   */
  public void setChassisSpeeds(ChassisSpeeds chassisSpeeds)
  {
    setModuleStates(DriveConstants.kDriveKinematics.toSwerveModuleStates(chassisSpeeds));
  }

  /**
   * Sets the swerve ModuleStates.
   *
   * @param desiredStates The desired SwerveModule states.
   */
  public void setModuleStates(SwerveModuleState[] desiredStates) {
    for(int i = 0; i < 4; ++i)
    {
      desiredStates[i].speedMetersPerSecond = desiredStates[i].speedMetersPerSecond*-1;
    }
    SwerveDriveKinematics.desaturateWheelSpeeds(
        desiredStates, DriveConstants.kMaxSpeedMetersPerSecond);
    m_frontLeft.setDesiredState(desiredStates[0], false);
    m_frontRight.setDesiredState(desiredStates[1], false);
    m_rearLeft.setDesiredState(desiredStates[2], false);
    m_rearRight.setDesiredState(desiredStates[3], false);
  }

  public SwerveModulePosition[] getModulePositions() {
    return new SwerveModulePosition[] {
            m_frontLeft.getPosition(),
            m_frontRight.getPosition(),
            m_rearLeft.getPosition(),
            m_rearRight.getPosition()
    };
  }

  /** Resets the drive encoders to currently read a position of 0. */
  public void resetEncoders() {
    m_frontLeft.resetEncoders();
    m_rearLeft.resetEncoders();
    m_frontRight.resetEncoders();
    m_rearRight.resetEncoders();
  }

  /** Zeroes the heading of the robot. */
  public void zeroHeading() {
    m_gyro.setOffset(m_gyro.getRawRotation3d());
    lastHeadingRadians = 0;
    resetOdometry(new Pose2d(getPose().getTranslation(), new Rotation2d()));
  }

  /**
   * Gets the current yaw angle of the robot, as reported by the imu. CCW positive, not wrapped.
   *
   * @return The yaw as a {@link Rotation2d} angle
   */
  public Rotation2d getYaw()
  {
      return DriveConstants.kGyroReversed
             ? Rotation2d.fromRadians(m_gyro.getRotation3d().unaryMinus().getZ())
             : Rotation2d.fromRadians(m_gyro.getRotation3d().getZ());
  }

  /**
   * Gets the current pitch angle of the robot, as reported by the imu.
   *
   * @return The heading as a {@link Rotation2d} angle
   */
  public Rotation2d getPitch()
  {
      return DriveConstants.kGyroReversed
             ? Rotation2d.fromRadians(m_gyro.getRotation3d().unaryMinus().getY())
             : Rotation2d.fromRadians(m_gyro.getRotation3d().getY());
  }

  /**
   * Gets the current roll angle of the robot, as reported by the imu.
   *
   * @return The heading as a {@link Rotation2d} angle
   */
  public Rotation2d getRoll()
  {
    // Read the imu if the robot is real or the accumulator if the robot is simulated.
      return DriveConstants.kGyroReversed
             ? Rotation2d.fromRadians(m_gyro.getRotation3d().unaryMinus().getX())
             : Rotation2d.fromRadians(m_gyro.getRotation3d().getX());
  }

   /**
   * Gets the current yaw velocity of the robot, as reported by the imu. CCW positive, not wrapped.
   *
   * @return The yaw velocity as an {@link Rotation2d} angular velocity
   */
  public Rotation2d getYawVel() {
    // Read the imu if the robot is real or the accumulator if the robot is simulated.
      return DriveConstants.kGyroReversed
          ? Rotation2d.fromRadians(getAngularVel().get().unaryMinus().getZ())
          : Rotation2d.fromRadians(getAngularVel().get().getZ());
  }

  /**
   * Gets the current pitch velocity of the robot, as reported by the imu.
   *
   * @return The pitch velocity as an {@link Rotation2d} angular velocity
   */
  public Rotation2d getPitchVel() {
    // Read the imu if the robot is real or the accumulator if the robot is simulated.
      return DriveConstants.kGyroReversed
          ? Rotation2d.fromRadians(getAngularVel().get().unaryMinus().getY())
          : Rotation2d.fromRadians(getAngularVel().get().getY());
  }

  /**
   * Gets the current roll velocity of the robot, as reported by the imu.
   *
   * @return The roll velocity as an {@link Rotation2d} angular velocity
   */
  public Rotation2d getRollVel() {
    // Read the imu if the robot is real or the accumulator if the robot is simulated.
      return DriveConstants.kGyroReversed
          ? Rotation2d.fromRadians(getAngularVel().get().unaryMinus().getX())
          : Rotation2d.fromRadians(getAngularVel().get().getX());
  }

  /**
   * Gets the current gyro {@link Rotation3d} of the robot, as reported by the imu.
   *
   * @return The heading as a {@link Rotation3d} angle
   */
  public Rotation3d getGyroRotation3d()
  {
    // Read the imu if the robot is real or the accumulator if the robot is simulated.
      return DriveConstants.kGyroReversed
             ? m_gyro.getRotation3d().unaryMinus()
             : m_gyro.getRotation3d();
  }

  /**
   * Gets current acceleration of the robot in m/s/s. If gyro unsupported returns empty.
   *
   * @return acceleration of the robot as a {@link Translation3d}
   */
  public Optional<Translation3d> getAccel()
  {
      return m_gyro.getAccel();
  }

  /**
   * Gets current angular velocity of the robot in rad/s. If gyro unsupported returns empty.
   *
   * @return angular velocity of the robot as a {@link Rotation3d}
   */
  public Optional<Rotation3d> getAngularVel()
  {
      return m_gyro.getAngularVel();
  }
  
  /**
   * Finds the average TX among non zero limelight values.
   * 
   * @return the average TX
   */
  public double getAverageSpeakerTX() {
    double leftTX = LimelightHelpers.getTX("limelight-left");
    double rightTX = LimelightHelpers.getTX("limelight-right");
    return leftTX==0 ? rightTX : (rightTX==0 ? leftTX : (leftTX+rightTX)/2);
  }

  /**
   * Returns the distance to our alliance's speaker.
   * 
   * @return the distance, in meters, to our speaker.
   */
  public double getDistanceToSpeaker()
  {
    boolean isBlue = DriverStation.getAlliance().orElse(DriverStation.Alliance.Blue).equals(DriverStation.Alliance.Blue);
    Translation2d speaker = isBlue ? FieldConstants.BLUE_SPEAKER : FieldConstants.RED_SPEAKER;
    Translation2d robotPosition = getPose().getTranslation();
    return speaker.getDistance(robotPosition);
  }
  
  /**
   * Returns what the position of the robot will be by the time it's note is in the speaker.
   * Useful for calculating what the angle should be to shoot a note while moving.
   * 
   * @param noteVelocity The velocity, in meters per second, the note is shot at.
   * @return The distance, in meters, to the speaker by the time the note is in.
   */
  public double getEffectiveDistanceToSpeaker(double noteVelocity)
  {
    boolean isBlue = DriverStation.getAlliance().orElse(DriverStation.Alliance.Blue).equals(DriverStation.Alliance.Blue);
    Translation2d speaker = isBlue ? FieldConstants.BLUE_SPEAKER : FieldConstants.RED_SPEAKER; // The location of the speaker.
    Translation2d robotPosition = getPose().getTranslation(); // The actual location of the robot.
    // The number of seconds it will take for the note to reach the speaker, using an approximation of what the angle will be.
    double secondsInAir = getDistanceToSpeaker()*Math.cos(Math.toRadians(AngleConstants.ANGLE_INTERPOLATION.get(getDistanceToSpeaker())))/noteVelocity; 
    ChassisSpeeds robotChassisSpeed = getChassisSpeeds().times(secondsInAir);
    // Where the robot will be when the note is in the speaker. Effectively where the note is shot from if the robot were still.
    Translation2d robotTranslation = new Translation2d(robotChassisSpeed.vxMetersPerSecond, robotChassisSpeed.vyMetersPerSecond);
    return speaker.getDistance(robotPosition.plus(robotTranslation)); // The distance between the effective robot position and the speaker.
  }

  /**
   * Returns whether or not the robot is pointed at the speaker.
   * 
   * @return whether or not the robot is pointed at the speaker.
   */
  public boolean facingSpeaker()
  {
    return getAverageSpeakerTX() < DriveConstants.kSpeakerTXTolerance;
  }

  /**
   * Set the heading angle for the swerve heading controller
   * 
   * @param angle Angle of the heading in radians
   */
  public void setHeadingAngle(double angle)
  {
    lastHeadingRadians = angle;
  }

  public void correctOdom(boolean force, boolean correctYaw) {
    double time = Timer.getFPGATimestamp();
    // Alliance alliance = DriverStation.getAlliance().get();
    Alliance alliance = DriverStation.Alliance.Blue;
    Pose2d leftBotPose = null;
    double leftPoseTimestamp =
        time
            - ((LimelightHelpers.getLatency_Capture("limelight-left")
                    + LimelightHelpers.getLatency_Pipeline("limelight-left"))
                / 1000.0);
    Pose2d rightBotPose = null;
    double rightPoseTimestamp =
        time
            - ((LimelightHelpers.getLatency_Capture("limelight-right")
                    + LimelightHelpers.getLatency_Pipeline("limelight-right"))
                / 1000.0);

    Pose2d robotAtLeftCapture = poseLookup.lookup(leftPoseTimestamp);
    Pose2d robotAtRightCapture = poseLookup.lookup(rightPoseTimestamp);

    if (LimelightHelpers.getTV("limelight-left")) {
      Pose2d botpose =
          (alliance == DriverStation.Alliance.Blue
              ? LimelightHelpers.getBotPose2d_wpiBlue("limelight-left")
              : LimelightHelpers.getBotPose2d_wpiRed("limelight-left"));

      if (botpose.getX() > 0.1
          && botpose.getX() < Constants.FieldConstants.FIELD_SIZE.getX() - 0.1
          && botpose.getY() > 0.1
          && botpose.getY() < Constants.FieldConstants.FIELD_SIZE.getY() - 1) {
        leftBotPose = botpose;
      }
    }

    if (LimelightHelpers.getTV("limelight-right")) {
      Pose2d botpose =
          (alliance == DriverStation.Alliance.Blue
              ? LimelightHelpers.getBotPose2d_wpiBlue("limelight-right")
              : LimelightHelpers.getBotPose2d_wpiRed("limelight-right"));

      if (botpose.getX() > 0.1
          && botpose.getX() < Constants.FieldConstants.FIELD_SIZE.getX() - 0.1
          && botpose.getY() > 0.1
          && botpose.getY() < Constants.FieldConstants.FIELD_SIZE.getY() - 1) {
        rightBotPose = botpose;
      }
    }

    Pose2d correctionPose = null;
    Pose2d robotAtCorrectionPose = null;
    double correctionTimestamp = 0;
    Matrix<N3, N1> correctionDevs = null;

    if (leftBotPose != null && rightBotPose != null) {
      // Left and right have poses
      Pose2d leftToRightDiff = leftBotPose.relativeTo(rightBotPose);
      if (leftToRightDiff.getTranslation().getNorm() < 0.3
          && (!correctYaw || Math.abs(leftToRightDiff.getRotation().getDegrees()) < 15)) {
        // They agree
        correctionPose = leftBotPose.interpolate(rightBotPose, 0.5);
        robotAtCorrectionPose = robotAtLeftCapture.interpolate(robotAtRightCapture, 0.5);
        correctionTimestamp = (leftPoseTimestamp + rightPoseTimestamp) / 2.0;
        correctionDevs = Constants.DriveConstants.visionStdDevsTrust;
      } else {
        // They don't agree
        Pose2d leftDiff = leftBotPose.relativeTo(robotAtLeftCapture);
        Pose2d rightDiff = rightBotPose.relativeTo(robotAtRightCapture);
        double leftDist = leftDiff.getTranslation().getNorm();
        double rightDist = rightDiff.getTranslation().getNorm();

        if ((leftDist < 2.0 || force) && leftDist <= rightDist) {
          // Left closest
          if (!correctYaw || force || Math.abs(leftDiff.getRotation().getDegrees()) < 15) {
            correctionPose = leftBotPose;
            robotAtCorrectionPose = robotAtLeftCapture;
            correctionTimestamp = leftPoseTimestamp;
            correctionDevs = Constants.DriveConstants.visionStdDevs;
          }
        } else if ((rightDist < 2.0 || force) && rightDist <= leftDist) {
          // Right closest
          if (!correctYaw || force || Math.abs(rightDiff.getRotation().getDegrees()) < 15) {
            correctionPose = rightBotPose;
            robotAtCorrectionPose = robotAtRightCapture;
            correctionTimestamp = rightPoseTimestamp;
            correctionDevs = Constants.DriveConstants.visionStdDevs;
          }
        }
      }
    } else if (leftBotPose != null) {
      Pose2d leftDiff = leftBotPose.relativeTo(robotAtLeftCapture);
      double leftDist = leftDiff.getTranslation().getNorm();

      if (leftDist < 2.0 || force) {
        if (!correctYaw || force || Math.abs(leftDiff.getRotation().getDegrees()) < 15) {
          correctionPose = leftBotPose;
          robotAtCorrectionPose = robotAtLeftCapture;
          correctionTimestamp = leftPoseTimestamp;
          correctionDevs = Constants.DriveConstants.visionStdDevs;
        }
      }
    } else if (rightBotPose != null) {
      Pose2d rightDiff = rightBotPose.relativeTo(robotAtRightCapture);
      double rightDist = rightDiff.getTranslation().getNorm();

      if (rightDist < 2.0 || force) {
        if (!correctYaw || force || Math.abs(rightDiff.getRotation().getDegrees()) < 15) {
          correctionPose = rightBotPose;
          robotAtCorrectionPose = robotAtRightCapture;
          correctionTimestamp = rightPoseTimestamp;
          correctionDevs = Constants.DriveConstants.visionStdDevs;
        }
      }
    }

    if (correctionPose != null) {
      m_poseEstimator.addVisionMeasurement(
          (correctYaw)
              ? correctionPose
              : new Pose2d(correctionPose.getTranslation(), robotAtCorrectionPose.getRotation()),
          correctionTimestamp,
          correctionDevs);
    }

    if (leftBotPose != null) {
      SmartDashboard.putNumberArray(
          "LLPoseLeft",
          new double[] {
            leftBotPose.getX(), leftBotPose.getY(), leftBotPose.getRotation().getDegrees()
          });
    }
    if (rightBotPose != null) {
      SmartDashboard.putNumberArray(
          "LLPoseRight",
          new double[] {
            rightBotPose.getX(), rightBotPose.getY(), rightBotPose.getRotation().getDegrees()
          });
    }
  }

}
