package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.IntakeConstants;
import frc.utils.MotorSetup;

/**
 * The subsystem controlling the intake of the robot. Has two motors and a sensor.
 */
public class IntakeSubsystem extends SubsystemBase {
    private final CANSparkMax m_leaderMotor;
    private final CANSparkMax m_followerMotor;
    private final DigitalInput m_noteSensor;

    /**
     * Creates a new IntakeSubsystem.
     * Instantiates the motors and digital input.
     */
    public IntakeSubsystem() {
        m_leaderMotor = new CANSparkMax(IntakeConstants.LEADER_MOTOR_ID, MotorType.kBrushless);
        m_followerMotor = new CANSparkMax(IntakeConstants.FOLLOWER_MOTOR_ID, MotorType.kBrushless);
        setUpMotors();

        m_noteSensor = new DigitalInput(IntakeConstants.SENSOR_ID);
    }

    /**
     * Sets many motor settings.
     */
    private void setUpMotors() {
        MotorSetup.setupLeaderAndFollowerMotors(
            m_leaderMotor, IntakeConstants.LEADER_INVERTED,
            m_followerMotor, true,
            IdleMode.kBrake,
            IntakeConstants.CURRENT_LIMIT,
            IntakeConstants.RAMP_RATE);
        
        MotorSetup.finalizeMotors(m_leaderMotor, m_followerMotor);
    }

    /**
     * Turns on the intake rollors.
     */
    public void intakeOn() {
        m_leaderMotor.set(IntakeConstants.INTAKE_SPEED);
    }

    /**
     * Turn off the intake rollors.
     */
    public void intakeOff() {
        m_leaderMotor.set(0);
    }

    /**
     * Returns the value of the note sesnor.
     * @return the value of the note sensor.
     */
    public boolean hasNote() {
        return m_noteSensor.get();
    }
}
