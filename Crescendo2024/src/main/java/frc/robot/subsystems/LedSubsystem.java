package frc.robot.subsystems;

import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.LEDConstants;

public class LedSubsystem extends SubsystemBase {
    private final AddressableLED m_leds = new AddressableLED(LEDConstants.LED_PORT);
    private final AddressableLEDBuffer m_buffer = new AddressableLEDBuffer(LEDConstants.FULL_LENGTH);
    public boolean m_readyToShoot = false;
    public double m_shooterRpmTarget = 0.0;
    public double m_shooterRpmCurrent = 0.0;
    public double m_intakeTimestamp = 0.0;

    public LedSubsystem()
    {
        m_leds.setLength(m_buffer.getLength());
        m_leds.setData(m_buffer);
        m_leds.start();
    }

    public void periodic()
    {
        if(((m_intakeTimestamp >= 0.1) && (Timer.getFPGATimestamp() - m_intakeTimestamp >= 1)) || m_readyToShoot)
        {
            setStrobe(Section.SHOOTER, Color.kGreen, LEDConstants.STROBE_FAST_DURATION);
        }
        else
        {
            m_intakeTimestamp = 0.0;
        }
        if(m_shooterRpmCurrent <= 100.0 && (m_intakeTimestamp <= 0.1))
        {
            setRainbow(Section.SHOOTER);
        }
        else if(!m_readyToShoot || Math.abs(m_shooterRpmCurrent - m_shooterRpmTarget) >= 1000.0)
        {
            int RPMperLED = (int)(m_shooterRpmTarget / (Section.SHOOTER.end()));
            double  ratio = m_shooterRpmCurrent /RPMperLED/Section.SHOOTER.end();
            double red = Color.kRed.red * (1 - ratio);
            double green = Color.kGreen.green * ratio;
            setGrowingSolid(Section.SHOOTER, new Color(red,green,0.0), (int) m_shooterRpmCurrent /RPMperLED);
        }
        m_leds.setData(m_buffer);
    }

    public void setIntakeSequence(BooleanSupplier hasNote, BooleanSupplier isTrackingNote)
    {
        if(!hasNote.getAsBoolean() && !isTrackingNote.getAsBoolean())
        {
            setSolid(Section.SHOOTER, Color.kRed);
        }
        else if(hasNote.getAsBoolean())
        {
            setStrobe(Section.SHOOTER, Color.kGreen, LEDConstants.STROBE_FAST_DURATION);
            m_intakeTimestamp = Timer.getFPGATimestamp();
        }
        else
        {
            setStrobe(Section.SHOOTER, Color.kRed, LEDConstants.STROBE_FAST_DURATION);
        }
    }

    public void setShooterState(DoubleSupplier shooterCurrentRPM, DoubleSupplier shooterTargetRPM, BooleanSupplier isReady)
    {
        m_shooterRpmCurrent = shooterCurrentRPM.getAsDouble();
        m_shooterRpmTarget = shooterTargetRPM.getAsDouble();
        m_readyToShoot = isReady.getAsBoolean();
    }


    // public void setFire(Section[] sections) {
    //     int cooldown;


    //     // Step 1.  Cool down every cell a little
    //     for(int sectionIndex = 0; sectionIndex < sections.length; ++sectionIndex)
    //     {
    //         Section section = sections[sectionIndex];
    //         for( int i = section.start(); i < section.end(); i++) {
    //         cooldown = random(0, ((LEDConstants.FIRE_COOLING * 10) / (section.start() - section.end())) + 2);

    //         if(cooldown>section[i]) {
    //             heat[i]=0;
    //         } else {
    //             heat[i]=heat[i]-cooldown;
    //         }
    //         }
    //     }

    //     // Step 2.  Heat from each cell drifts 'up' and diffuses a little
    //     for( int k= NUM_LEDS - 1; k >= 2; k--) {
    //       heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2]) / 3;
    //     }

    //     // Step 3.  Randomly ignite new 'sparks' near the bottom
    //     if( random(255) < Sparking ) {
    //       int y = random(7);
    //       heat[y] = heat[y] + random(160,255);
    //       //heat[y] = random(160,255);
    //     }

    //     // Step 4.  Convert heat to LED colors
    //     for( int j = 0; j < NUM_LEDS - MIRROR; j++) {
    //       setPixelHeatColor(j+MIRROR, heat[j] );
    //       setPixelHeatColor(MIRROR-j, heat[j] );
    //       }

    //     showStrip();
    //     delay(SpeedDelay);
    //   }

    //   void setPixelHeatColor (int Pixel, byte temperature) {
    //     // Scale 'heat' down from 0-255 to 0-191
    //     byte t192 = round((temperature/255.0)*191);

    //     // calculate ramp up from
    //     byte heatramp = t192 & 0x3F; // 0..63
    //     heatramp <<= 2; // scale up to 0..252

    //     // figure out which third of the spectrum we're in:
    //     if( t192 > 0x80) {                     // hottest
    //       setPixel(Pixel, 255, 255, heatramp);
    //     } else if( t192 > 0x40 ) {             // middle
    //       setPixel(Pixel, 255, heatramp, 0);
    //     } else {                               // coolest
    //       setPixel(Pixel, heatramp, 0, 0);
    //     }
    //   }

    public void setGrowingSolid(Section section, Color color, int offset)
    {
        if (color == null)
        {
            switch(DriverStation.getAlliance().get())
            {
                case Red:
                    color = Color.kRed;
                case Blue:
                    color = Color.kBlue;
                default:
                    color = Color.kOrange;
            }
        }
        for(int i = section.start(); i < section.start()+offset; ++i)
        {
            m_buffer.setLED(i, color);
            m_buffer.setLED(section.end()-i, color);
        }
    }

    public void setIncreasingSolid(Section section, Color color, int finalLED)
    {
        if (color == null)
        {
            switch(DriverStation.getAlliance().get())
            {
                case Red:
                    color = Color.kRed;
                case Blue:
                    color = Color.kBlue;
                default:
                    color = Color.kOrange;
            }
        }
        for(int i = section.start(); i < section.start()+finalLED; i++)
        {
            m_buffer.setLED(i, color);
        }
    }

    public void setDecreasingSolid(Section section, Color color, int startLED)
    {
        if (color == null)
        {
            switch(DriverStation.getAlliance().get())
            {
                case Red:
                    color = Color.kRed;
                case Blue:
                    color = Color.kBlue;
                default:
                    color = Color.kOrange;
            }
        }
        for(int i = section.start()+startLED; i < section.end(); i++)
        {
            m_buffer.setLED(i, color);
        }
    }

    public void setMultipleIncreasingSolid(Section[] sections, Color color, int finalLED)
    {
        for(Section section : sections)
        {
            setIncreasingSolid(section, color, finalLED);
        }
    }

    public void setMultipleDecreasingSolid(Section[] sections, Color color, int startLED)
    {
        for(Section section : sections)
        {
            setIncreasingSolid(section, color, startLED);
        }
    }

    public void setSolid(Section section, Color color)
    {
        if (color == null)
        {
            switch(DriverStation.getAlliance().get())
            {
                case Red:
                    color = Color.kRed;
                case Blue:
                    color = Color.kBlue;
                default:
                    color = Color.kOrange;
            }
        }
        for(int i = section.start(); i < section.end(); i++)
        {
            m_buffer.setLED(i, color);
        }
    }

    public void setGrowingStrobe(Section section, Color color, int offset, double duration)
    {
        if (color == null)
        {
            switch(DriverStation.getAlliance().get())
            {
                case Red:
                    color = Color.kRed;
                case Blue:
                    color = Color.kBlue;
                default:
                    color = Color.kOrange;
            }
        }
        boolean on = ((Timer.getFPGATimestamp() % duration) / duration) > 0.5;
        color = on ? color : Color.kBlack;
        for(int i = section.start(); i < section.start()+offset; ++i)
        {
            m_buffer.setLED(i, color);
            m_buffer.setLED(section.end()-i, color);
        }
    }

    public void setIncreasingStrobe(Section section, Color color, int finalLED, double duration)
    {
        if (color == null)
        {
            switch(DriverStation.getAlliance().get())
            {
                case Red:
                    color = Color.kRed;
                case Blue:
                    color = Color.kBlue;
                default:
                    color = Color.kOrange;
            }
        }
        boolean on = ((Timer.getFPGATimestamp() % duration) / duration) > 0.5;
        color = on ? color : Color.kBlack;
        for(int i = section.start(); i < section.start()+finalLED; i++)
        {
            m_buffer.setLED(i, color);
        }
    }

    public void setDecreasingStrobe(Section section, Color color, int startLED, double duration)
    {
        if (color == null)
        {
            switch(DriverStation.getAlliance().get())
            {
                case Red:
                    color = Color.kRed;
                case Blue:
                    color = Color.kBlue;
                default:
                    color = Color.kOrange;
            }
        }
        boolean on = ((Timer.getFPGATimestamp() % duration) / duration) > 0.5;
        color = on ? color : Color.kBlack;
        for(int i = section.start()+startLED; i < section.end(); i++)
        {
            m_buffer.setLED(i, color);
        }
    }

    public void setMultipleIncreasingStrobe(Section[] sections, Color color, int finalLED, double duration)
    {
        for(Section section : sections)
        {
            setIncreasingStrobe(section, color, finalLED, duration);
        }
    }

    public void setMultipleDecreasingStrobe(Section[] sections, Color color, int startLED, double duration)
    {
        for(Section section : sections)
        {
            setDecreasingStrobe(section, color, startLED, duration);
        }
    }

    public void setStrobe(Section section, Color color, double duration)
    {
        boolean on = ((Timer.getFPGATimestamp() % duration) / duration) > 0.5;
        setSolid(section, on ? color : Color.kBlack);
    }

    public void setBreath(Section section, Color c1, Color c2, double duration)
    {
        setBreath(section, c1, c2, duration, Timer.getFPGATimestamp());
    }

    public void setGrowingBreath(Section section, Color c1, Color c2, int offset, double duration)
    {
        double timestamp = Timer.getFPGATimestamp();
        double x = ((timestamp % LEDConstants.BREATH_DURATION) / LEDConstants.BREATH_DURATION) * 2.0 * Math.PI;
        double ratio = (Math.sin(x) + 1.0) / 2.0;
        double red = (c1.red * (1 - ratio)) + (c2.red * ratio);
        double green = (c1.green * (1 - ratio)) + (c2.green * ratio);
        double blue = (c1.blue * (1 - ratio)) + (c2.blue * ratio);
        setGrowingSolid(section, new Color(red, green, blue), offset);
    }

    public void setBreath(Section section, Color c1, Color c2, double duration, double timestamp)
    {
        double x = ((timestamp % LEDConstants.BREATH_DURATION) / LEDConstants.BREATH_DURATION) * 2.0 * Math.PI;
        double ratio = (Math.sin(x) + 1.0) / 2.0;
        double red = (c1.red * (1 - ratio)) + (c2.red * ratio);
        double green = (c1.green * (1 - ratio)) + (c2.green * ratio);
        double blue = (c1.blue * (1 - ratio)) + (c2.blue * ratio);
        setSolid(section, new Color(red, green, blue));
    }

    public void setGrowingWave(Section section, Color c1, Color c2, int offset, double cycleLength, double duration) {
        double x = (1 - ((Timer.getFPGATimestamp() % duration) / duration)) * 2.0 * Math.PI;
        double xDiffPerLed = (2.0 * Math.PI) / cycleLength;
        for (int i = 0; i < section.end(); i++) {
            x += xDiffPerLed;
            if (i >= section.start()+offset && i<= section.end()-offset) {
                double ratio = (Math.pow(Math.sin(x), LEDConstants.WAVE_EXPONENT) + 1.0) / 2.0;
                if (Double.isNaN(ratio)) {
                    ratio = (-Math.pow(Math.sin(x + Math.PI), LEDConstants.WAVE_EXPONENT) + 1.0) / 2.0;
                }
                if (Double.isNaN(ratio)) {
                    ratio = 0.5;
                }
                double red = (c1.red * (1 - ratio)) + (c2.red * ratio);
                double green = (c1.green * (1 - ratio)) + (c2.green * ratio);
                double blue = (c1.blue * (1 - ratio)) + (c2.blue * ratio);
                m_buffer.setLED(i, new Color(red, green, blue));
            }
        }
    }

    public void setWave(Section section, Color c1, Color c2, double cycleLength, double duration) {
        double x = (1 - ((Timer.getFPGATimestamp() % duration) / duration)) * 2.0 * Math.PI;
        double xDiffPerLed = (2.0 * Math.PI) / cycleLength;
        for (int i = 0; i < section.end(); i++) {
            x += xDiffPerLed;
            if (i >= section.start()) {
                double ratio = (Math.pow(Math.sin(x), LEDConstants.WAVE_EXPONENT) + 1.0) / 2.0;
                if (Double.isNaN(ratio)) {
                    ratio = (-Math.pow(Math.sin(x + Math.PI), LEDConstants.WAVE_EXPONENT) + 1.0) / 2.0;
                }
                if (Double.isNaN(ratio)) {
                    ratio = 0.5;
                }
                double red = (c1.red * (1 - ratio)) + (c2.red * ratio);
                double green = (c1.green * (1 - ratio)) + (c2.green * ratio);
                double blue = (c1.blue * (1 - ratio)) + (c2.blue * ratio);
                m_buffer.setLED(i, new Color(red, green, blue));
            }
        }
    }

    public void setStripes(Section section, List<Color> colors, int length, double duration) {
        int offset = (int) (Timer.getFPGATimestamp() % duration / duration * length * colors.size());
        for (int i = section.start(); i < section.end(); i++) {
            int colorIndex =
                    (int) (Math.floor((double) (i - offset) / length) + colors.size()) % colors.size();
            colorIndex = colors.size() - 1 - colorIndex;
            m_buffer.setLED(i, colors.get(colorIndex));
        }
    }

    public void setGrowingRainbow(Section section, int offset)
    {
        double x = (1 - ((Timer.getFPGATimestamp() / LEDConstants.RAINBOW_DURATION) % 1.0)) * 180.0;
        double xDiffPerLed = 180.0 / LEDConstants.RAINBOW_CYCLE_LENGTH;
        for (int i = 0; i < section.end(); i++)
        {
            x += xDiffPerLed;
            x %= 180.0;
            if (i >= section.start()+offset && i<= section.end()-offset)
            {
                m_buffer.setHSV(i, (int) x, 255, 255);
            }
        }
    }

    public void setRainbow(Section section)
    {
        double x = (1 - ((Timer.getFPGATimestamp() / LEDConstants.RAINBOW_DURATION) % 1.0)) * 180.0;
        double xDiffPerLed = 180.0 / LEDConstants.RAINBOW_CYCLE_LENGTH;
        for (int i = 0; i < section.end(); i++)
        {
            x += xDiffPerLed;
            x %= 180.0;
            if (i >= section.start() && i<= section.end())
            {
                m_buffer.setHSV(i, (int) x, 255, 255);
            }
        }
    }

    // public void setRuckusHold(Section section, Color color, int offset)
    // {
    //   setRuckusHold(section, color, offset, Timer.getFPGATimestamp());
    // }

    // //Breaths in and out starting at the center of the section provided and grows larger by size of offset (Look at ruckusBot)
    // public void setRuckusHold(Section section, Color color, int offset, double timestamp)
    // {
    //     double x = ((Timer.getFPGATimestamp() % LEDConstants.BREATH_DURATION) / LEDConstants.BREATH_DURATION) * 2.0 * Math.PI;
    //     double ratio = (Math.sin(x) + 1.0) / 2.0;
    //     double red = (color.red * (1 - ratio)) + (Color.kBlack.red * ratio);
    //     double green = (color.green * (1 - ratio)) + (Color.kBlack.green * ratio);
    //     double blue = (color.blue * (1 - ratio)) + (Color.kBlack.blue * ratio);
    //     color = new Color(red, green, blue);
    //     for (int i = section.start(); i < section.end(); i++)
    //     {
    //         if(((i%15) >= (7-offset)) && ((i%15) <=(7+offset)))
    //         {
    //           buffer.setLED(i, color);
    //         }
    //         else
    //         {
    //           buffer.setLED(i,Color.kBlack);
    //         }

    //     }
    // }

    public static enum Section {
        SHOOTER_RIGHT,
        SHOOTER_CENTER,
        SHOOTER_LEFT,
        SHOOTER,
        INTAKE,
        FULL;

        private int start() {
            switch (this) {
                case SHOOTER_RIGHT:
                    return 0;
                case SHOOTER_CENTER:
                    return LEDConstants.SHOOTER_STRIP_LENGTH;
                case SHOOTER_LEFT:
                    return SHOOTER_CENTER.start() + LEDConstants.SHOOTER_STRIP_LENGTH;
                case SHOOTER:
                    return 0;
                case INTAKE:
                    return LEDConstants.SHOOTER_LENGTH;
                case FULL:
                    return 0;
                default:
                    return 0;
            }
        }

        private int end() {
            switch (this) {
                case SHOOTER_RIGHT:
                    return LEDConstants.SHOOTER_STRIP_LENGTH;
                case SHOOTER_CENTER:
                    return SHOOTER_RIGHT.end() + LEDConstants.SHOOTER_STRIP_LENGTH;
                case SHOOTER_LEFT:
                    return SHOOTER_CENTER.end() + LEDConstants.SHOOTER_STRIP_LENGTH;
                case SHOOTER:
                    return LEDConstants.SHOOTER_LENGTH;
                case INTAKE:
                    return LEDConstants.FULL_LENGTH - LEDConstants.SHOOTER_LENGTH;
                case FULL:
                    return LEDConstants.FULL_LENGTH;
                default:
                    return LEDConstants.FULL_LENGTH;
            }
        }
    }

}
