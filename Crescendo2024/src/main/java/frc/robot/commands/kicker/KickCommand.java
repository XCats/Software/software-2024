package frc.robot.commands.kicker;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.KickerSubsystem;

public class KickCommand extends Command {
    
    KickerSubsystem m_kickerSubsystem;

    public KickCommand (KickerSubsystem kickerSubsystem)
    {
        super.addRequirements(kickerSubsystem);
        m_kickerSubsystem = kickerSubsystem;
    }

    @Override
    public void initialize()
    {
        m_kickerSubsystem.kickerOn();
    }

    @Override
    public void execute()
    {

    }

    @Override
    public void end (boolean interrupted)
    {
        m_kickerSubsystem.kickerOff();
    }

    @Override
    public boolean isFinished()
    {
        return false;
    }
}
