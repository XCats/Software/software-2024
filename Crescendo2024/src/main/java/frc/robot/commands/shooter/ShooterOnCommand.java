package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants.ShooterConstants;
import frc.robot.subsystems.ShooterSubsystem;

/**
 * A command that turns on the shooter.
 */
public class ShooterOnCommand extends Command {
    ShooterSubsystem m_shooterSubsystem;

    public ShooterOnCommand(ShooterSubsystem shooterSubsystem) {
        super.addRequirements(shooterSubsystem);
        
        m_shooterSubsystem = shooterSubsystem;
    }

    @Override
    public void initialize() {
        m_shooterSubsystem.setPower(ShooterConstants.POWER);
    }

    @Override
    public void execute() {

    }

    @Override
    public void end(boolean interrupted) {

    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
