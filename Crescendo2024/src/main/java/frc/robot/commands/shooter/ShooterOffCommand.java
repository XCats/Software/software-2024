package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ShooterSubsystem;

/**
 * A command that turns off the shooter.
 */
public class ShooterOffCommand extends Command {
    ShooterSubsystem m_shooterSubsystem;

    public ShooterOffCommand(ShooterSubsystem shooterSubsystem) {
        super.addRequirements(shooterSubsystem);
        
        m_shooterSubsystem = shooterSubsystem;
    }

    @Override
    public void initialize() {
        m_shooterSubsystem.shooterOff();
    }

    @Override
    public void execute() {

    }

    @Override
    public void end(boolean interrupted) {

    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
