package frc.robot.commands.conveyor;

import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.NetworkTableValue;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ConveyorSubsytem;

public class ConveyCommand extends Command {

    ConveyorSubsytem conveyorSubsytem;

    public ConveyCommand(ConveyorSubsytem userConveyorSubsytem) 
    {
        conveyorSubsytem = userConveyorSubsytem;
        this.addRequirements(conveyorSubsytem);
    }

    @Override
    public void initialize()
    {
        conveyorSubsytem.convey();
        NetworkTableInstance
            .getDefault()
            .getTable("conveyor")
            .putValue("conveyorOn", NetworkTableValue.makeBoolean(true));
    }


    @Override
    public void execute() 
    {

    }

    @Override
    public void end(boolean interrupted)
    {
        conveyorSubsytem.stop();
    }

    @Override
    public boolean isFinished()
    {
        return false;
    }
}