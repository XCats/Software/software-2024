package frc.robot.commands.intake;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.IntakeSubsystem;

public class IntakeCommand extends Command {

    IntakeSubsystem m_intakeSubsystem;

    public IntakeCommand(IntakeSubsystem intakeSusbsystem) {
        super.addRequirements(intakeSusbsystem);
        
        m_intakeSubsystem = intakeSusbsystem;
    }

    @Override
    public void initialize() {
        m_intakeSubsystem.intakeOn();
    }

    @Override
    public void execute() {

    }

    @Override
    public void end(boolean interrupted) {
        m_intakeSubsystem.intakeOff();
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
