package frc.robot.commands.angle;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants.AngleConstants;
import frc.robot.Constants.ShooterConstants;
import frc.robot.subsystems.AngleSubsystem;
import frc.robot.subsystems.ShooterSubsystem;

/**
 * A command that sets the angle to aim at the speaker.
 */
public class SetSmartAngleCommand extends Command {
    AngleSubsystem m_angleSubsystem;
    DoubleSupplier m_distance;
    
    /**
     * Makes a new SetSmartAngleCommand.
     * 
     * @param angleSubsystem the AngleSubsystem to use
     * @param distance the distance, in meters, from the speaker.
     */
    public SetSmartAngleCommand(AngleSubsystem angleSubsystem, DoubleSupplier distance) {
        super.addRequirements(angleSubsystem);
        
        m_angleSubsystem = angleSubsystem;
        m_distance = distance;
    }

    @Override
    public void initialize() {
        m_angleSubsystem.setAngle(AngleConstants.ANGLE_INTERPOLATION.get(m_distance.getAsDouble()));;
    }

    @Override
    public void execute() {

    }

    @Override
    public void end(boolean interrupted) {

    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
