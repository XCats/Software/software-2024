package frc.robot.commands.climber;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ClimberSubsystem;

public class ClimberUpCommand  extends Command {
    
    ClimberSubsystem m_climberSubsystem;

    public ClimberUpCommand (ClimberSubsystem climberSubsystem)
    {
        super.addRequirements(climberSubsystem);

        m_climberSubsystem = climberSubsystem;
    }

    @Override
    public void initialize()
    {
        m_climberSubsystem.climberUp();
    }

    @Override
    public void execute() 
    {
    }

    @Override
    public void end(boolean interrupted)
    {
        m_climberSubsystem.climberOff();
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
