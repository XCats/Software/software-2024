package frc.robot.commands.climber;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.ClimberSubsystem;

public class ClimberDownCommand extends Command {
    
    ClimberSubsystem m_climberSubsystem;

    public ClimberDownCommand (ClimberSubsystem climberSubsystem)
    {
        super.addRequirements(climberSubsystem);

        m_climberSubsystem = climberSubsystem;
    }

    @Override
    public void initialize()
    {
        m_climberSubsystem.climberDown();
    }

    @Override
    public void execute() 
    {
    }

    @Override
    public void end(boolean interrupted)
    {
        m_climberSubsystem.climberOff();
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
