package frc.robot.commands.factories;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.commands.conveyor.ConveyCommand;
import frc.robot.commands.intake.IntakeCommand;
import frc.robot.commands.kicker.KickCommand;
import frc.robot.subsystems.ConveyorSubsytem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.KickerSubsystem;

public class IntakeCommandFactory {

    IntakeSubsystem m_intakeSubsystem;
    ConveyorSubsytem m_conveyorSubsystem;
    KickerSubsystem m_kickerSubsystem;

    public IntakeCommandFactory(IntakeSubsystem intakeSubsystem, ConveyorSubsytem conveyorSubsytem, KickerSubsystem kickerSubsystem) {
        m_intakeSubsystem = intakeSubsystem;
        m_conveyorSubsystem = conveyorSubsytem;
        m_kickerSubsystem = kickerSubsystem;
    }

    public Command build() {
        return (new IntakeCommand(m_intakeSubsystem) 
          .alongWith(new ConveyCommand(m_conveyorSubsystem))
          .alongWith(new KickCommand(m_kickerSubsystem)))
        .until(m_conveyorSubsystem::noteInConveyor)
        .andThen(new IntakeCommand(m_intakeSubsystem) 
          .alongWith(new ConveyCommand(m_conveyorSubsystem))
          .alongWith(new KickCommand(m_kickerSubsystem))
        .until(m_conveyorSubsystem::noteNotInConveyor));
    }
}

