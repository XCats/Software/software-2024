package frc.robot.commands.factories;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.PrintCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc.robot.Constants.ShooterConstants;
import frc.robot.commands.angle.SetSmartAngleCommand;
import frc.robot.commands.kicker.KickCommand;
import frc.robot.commands.shooter.ShooterOffCommand;
import frc.robot.commands.shooter.ShooterOnCommand;
import frc.robot.subsystems.AngleSubsystem;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.KickerSubsystem;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.VisionSubsystem;

public class ShooterCommandFactory {
    private ShooterSubsystem m_shooter;
    private KickerSubsystem m_kicker;
    private AngleSubsystem m_angle;
    private DriveSubsystem m_drive;
    private VisionSubsystem m_vision;

    public ShooterCommandFactory(ShooterSubsystem shooter, KickerSubsystem kicker, AngleSubsystem angle, DriveSubsystem drive, VisionSubsystem vision) {
        m_shooter = shooter;
        m_kicker = kicker;
        m_angle = angle;
        m_drive = drive;
        m_vision = vision;
    }

    public Command shootNote() {
        return new ShooterOnCommand(m_shooter)
            .andThen(new WaitUntilCommand(m_shooter::upToSpeed))
            .andThen(new KickCommand(m_kicker)
                .until(m_shooter::shot))
            .andThen(new ShooterOffCommand(m_shooter));
    }

    public Command shootInSpeaker() {
        return new InstantCommand(() -> m_vision.setSpeakerTracking())
                .andThen(new SetSmartAngleCommand(m_angle, () -> m_drive.getEffectiveDistanceToSpeaker(ShooterConstants.NOTE_VELOCITY)))
                        .andThen(new WaitUntilCommand(m_angle::atTarget))
                    .alongWith(new RunCommand(() -> m_drive.drive(0, 0, 0, false, true, false, true), m_drive))
                        .until(m_drive::facingSpeaker)
                .andThen(shootNote())
                .andThen(() -> m_vision.setPoseEstimation());
    }
}
