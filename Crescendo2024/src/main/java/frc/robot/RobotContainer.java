// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.auto.NamedCommands;
import com.pathplanner.lib.commands.PathPlannerAuto;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants.AngleConstants;
import frc.robot.Constants.OIConstants;
import frc.robot.Constants.ShooterConstants;
import frc.robot.commands.conveyor.ConveyCommand;
import frc.robot.commands.factories.IntakeCommandFactory;
import frc.robot.commands.factories.ShooterCommandFactory;
import frc.robot.commands.intake.IntakeCommand;
import frc.robot.commands.kicker.KickCommand;
import frc.robot.commands.shooter.ShooterOnCommand;
import frc.robot.subsystems.AngleSubsystem;
import frc.robot.subsystems.ConveyorSubsytem;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.KickerSubsystem;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.VisionSubsystem;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.PrintCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;

/*
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems
  public static final DriveSubsystem m_robotDrive = new DriveSubsystem();
  public static final IntakeSubsystem m_intakeSubsystem = new IntakeSubsystem();
  public static final AngleSubsystem m_angleSubsystem = new AngleSubsystem();
  public static final ShooterSubsystem m_shooterSubsystem = new ShooterSubsystem();
  public static final ConveyorSubsytem m_conveyorSubsystem = new ConveyorSubsytem();
  public static final KickerSubsystem m_kickerSubsystem = new KickerSubsystem();
  public static final VisionSubsystem m_visionSubsystem = new VisionSubsystem();

  public static final IntakeCommandFactory m_intakeCommandFactory = new IntakeCommandFactory(m_intakeSubsystem, m_conveyorSubsystem, m_kickerSubsystem);
  public static final ShooterCommandFactory m_shooterCommandFactory = new ShooterCommandFactory(m_shooterSubsystem, m_kickerSubsystem, m_angleSubsystem, m_robotDrive, m_visionSubsystem);

  // The driver controllers
  CommandXboxController m_driver = new CommandXboxController(OIConstants.kDriverControllerPort);
  CommandXboxController m_operator = new CommandXboxController(OIConstants.kOperatorControllerPort);


  //Autonomous Sendable Chooser
  private final SendableChooser<Command> autoChooser;

  /**
   * The container for the robot. Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    // Name comands for pathplanner
    NamedCommands.registerCommand("Intake Command", m_intakeCommandFactory.build());

    // Configure the button bindings
    configureButtonBindings();
 // Name comands for pathplanner
    NamedCommands.registerCommand("Intake Command", new IntakeCommand(m_intakeSubsystem));


    // Configure default commands
    m_robotDrive.setDefaultCommand(
            // The left stick controls translation of the robot.
            // Turning is controlled by the X axis of the right stick.
            new RunCommand(
                    () -> m_robotDrive.drive(
                            -MathUtil.applyDeadband(m_driver.getLeftY(), OIConstants.kDriveDeadband),
                            -MathUtil.applyDeadband(m_driver.getLeftX(), OIConstants.kDriveDeadband),
                            -MathUtil.applyDeadband(m_driver.getRightX(), OIConstants.kDriveDeadband),
                            true, true, m_driver.leftTrigger().getAsBoolean(),
                            m_driver.leftBumper().getAsBoolean()),
                    m_robotDrive));

    autoChooser = AutoBuilder.buildAutoChooser();

    // The shooter is on a toggle, starting off.
    m_shooterSubsystem.setDefaultCommand(
            m_shooterSubsystem.stopShooterCommand()
    );

    SmartDashboard.putData(autoChooser);
  }

  
  /**
   * Use this method to define your button->command mappings. Buttons can be
   * created by
   * instantiating a {@link edu.wpi.first.wpilibj.GenericHID} or one of its
   * subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link CommandXboxController})
   */
  private void configureButtonBindings() {

    m_operator.povUp()
      .onTrue(m_angleSubsystem.setAngleCommand(AngleConstants.SUBWOOFER_ANGLE));
    m_operator.povLeft()
      .onTrue(m_angleSubsystem.setAngleCommand(AngleConstants.PODIUM_ANGLE));
    m_operator.povDown()
      .onTrue(m_angleSubsystem.setAngleCommand(AngleConstants.WING_ANGLE));
    m_operator.povRight()
      .onTrue(m_angleSubsystem.setAngleCommand(AngleConstants.AMP_ANGLE));

    //Driver controller
    m_driver.x().whileTrue(new RunCommand(
            () -> m_robotDrive.setX(),
            m_robotDrive));

    m_driver.b().onTrue(new InstantCommand(
       () -> m_robotDrive.zeroHeading(),
       m_robotDrive));
    
    m_driver.y().toggleOnTrue(new ShooterOnCommand(m_shooterSubsystem));

    m_driver.rightTrigger().whileTrue(new KickCommand(m_kickerSubsystem));
    
    m_driver.leftTrigger().whileTrue(m_intakeCommandFactory.build());

    m_driver.leftBumper()
      .whileTrue(m_angleSubsystem.setSmartAngleCommand(() -> m_robotDrive.getEffectiveDistanceToSpeaker(ShooterConstants.NOTE_VELOCITY)))
      .onTrue(new InstantCommand(() -> m_visionSubsystem.setSpeakerTracking()))
      .onFalse(new InstantCommand(() -> m_visionSubsystem.setPoseEstimation()));

     m_driver.povUp().onTrue(new InstantCommand(() -> m_robotDrive.setHeadingAngle(Math.round(m_robotDrive.getYaw().getRadians() / (2.0*Math.PI)) * Math.PI * 2)));
     m_driver.povDown().onTrue(new InstantCommand(() -> m_robotDrive.setHeadingAngle(Math.round(m_robotDrive.getYaw().getRadians() / (2.0*Math.PI)) * Math.PI * 2 - Math.PI)));
     m_driver.povLeft().onTrue(new InstantCommand(() -> m_robotDrive.setHeadingAngle(Math.round(m_robotDrive.getYaw().getRadians() / (2.0*Math.PI)) * Math.PI * 2 - Math.PI/2)));
     m_driver.povRight().onTrue(new InstantCommand(() -> m_robotDrive.setHeadingAngle(Math.round(m_robotDrive.getYaw().getRadians() / (2.0*Math.PI)) * Math.PI * 2 + Math.PI/2)));

    m_operator.leftBumper().onTrue(m_shooterCommandFactory.shootInSpeaker());
  }

  public Command getAutoCommand() {
      return autoChooser.getSelected();
  }

}
